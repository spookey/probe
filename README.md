# Probe

**Challenge:**

- There is the need to toggle one or more pins to ground from regular
  PC hardware.
- Also digital and analog sources need to be read.

**Solution:**

- Using some USB-GPIO board?
  - They are very rare and/or expensive.
  - Driver situation unknown.
    - It says Linux is somehow supported, but who knows...
    - Situation on BSD even more unclear...

- Using some Arduino Nano!
  - It is super cheap, already have one lying around.
  - Write some software, no problem, can do!

Here we go!

## Interface

The *Nano* listens on the serial port with a `115200` - `8N1` connection and
awaits input.

It is possible to control 4 pins, use `3f` to put #3 on (**full**) or `3n` to
put it off (**null**).

Every input yields a response with the current status
(pin readings or toggles).
It is beautiful, hand crafted json, deliciously served in one line with joy.

## Control

The raw serial interface may be a little tedious to use.
Besides - when reconnecting to the *Nano* via serial port,
it will reset itself. This causes all sockets to reset, leaving them in
unclear state.

There is the *Host* project that spins up some web application and keeping
the serial connection open all the time.

It keeps a timetable with the columns `flip`, `day` (of week), `hour`, `minute`
and `target`.

Some worker thread looks up the most recent entry and sets the state on
the Nano accordingly in regular intervals.

- `/api/serial/state`
  - Proxy of the current state of the *Nano*.
- `/api/glance/all`
  - Obtain a full dump of the timetable, or restore/overwrite it there.
- `/api-docs` or `/swagger-ui/index.html`
  - Some swagger interface for the api endpoints.
- `/`
  - The main web interface to comfortably configure the timetable.

## Deployment

Hardware has been soldered like this:

|           | **Pin**   | **Color**  |
| --------: | :-------: | :--------- |
| **Power** |           |            |
|           | `5V`      | Red        |
|           | `GND`     | Blue       |
| **Flip**  |           |            |
|           | `D2`      | Green      |
|           | `D3`      | Orange     |
|           | `D4`      | Violet     |
|           | `D5`      | Yellow     |
| **Read**  |           |            |
|           | `D7`      | Brown      |
|           | `D8`      | Grey       |
| **Sens**  |           |            |
|           | `A1`      | Green      |
|           | `A2`      | Blue       |

## Database

Bootstrap database with `mysql -u root -p`:

Create schema:

```sql
CREATE SCHEMA `probe` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
```

Create user:

```sql
CREATE USER `probe`@`localhost` IDENTIFIED BY 'very-secure';
```

Grant rights:

```sql
GRANT ALL PRIVILEGES ON `probe` . * TO `probe`@`localhost`;

FLUSH PRIVILEGES;
```
