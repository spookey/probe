#include "ctrl.hpp"

Ctrl::Sym::Sym(String name)
: name(name) {}

Ctrl::Dev::Dev(const uint8_t pin, String name)
: Sym(name), pin(pin) {}


Ctrl::Read::Read(const uint8_t pin, String name)
: Dev(pin, name) {
    pinMode(this->pin, INPUT);
}
boolean Ctrl::Read::get() {
    return HIGH == digitalRead(this->pin);
}
String Ctrl::Read::str() {
    return this->get() ? "true" : "false";
}


Ctrl::Sens::Sens(const uint8_t pin, String name)
: Dev(pin, name) {
    pinMode(this->pin, INPUT);
}
uint16_t Ctrl::Sens::get() {
    return (uint16_t) analogRead(this->pin);
}
String Ctrl::Sens::str() {
    return String(this->get(), DEC);
}


Ctrl::Flip::Flip(const uint8_t pin, String name, const boolean _flip)
: Dev(pin, name), _flip(_flip), state(false) {
    pinMode(this->pin, OUTPUT);
    this->set(false);
}
void Ctrl::Flip::set(const boolean state) {
    digitalWrite(this->pin, (state == this->_flip) ? LOW : HIGH);
    this->state = state;
}
boolean Ctrl::Flip::get() {
    return this->state;
}
String Ctrl::Flip::str() {
    return this->get() ? "true" : "false";
}


Ctrl::UNum::UNum(String name)
: Sym(name) {}
String Ctrl::UNum::str() {
    char res[1 + 8] = {CHAR_IGNORE};
    unsigned long sec = millis() / 1000;
    snprintf(res, 8, "%ld", sec);
    return String(res);
}


Ctrl::UStr::UStr(String name)
: Sym(name) {}
String Ctrl::UStr::get() {
    char res[1 + 16] = {CHAR_IGNORE};
    unsigned long sec = millis() / 1000;
    unsigned long min = sec / 60;
    unsigned long hrs = min / 60;
    unsigned long day = hrs / 24;
    sec = sec - (min * 60);
    min = min - (hrs * 60);
    hrs = hrs - (day * 24);
    if (0 < day) {
        snprintf(res, 16, "%ldd %02ld:%02ld:%02ld", day, hrs, min, sec);
    } else {
        snprintf(res, 16,      "%02ld:%02ld:%02ld",      hrs, min, sec);
    }
    return String(res);
}
String Ctrl::UStr::str() {
    return String("\"" + this->get() + "\"");
}
