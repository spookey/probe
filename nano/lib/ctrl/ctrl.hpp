#ifndef _ctrl_hpp_
#define _ctrl_hpp_

#include "init.hpp"
#include "comm.hpp"

class Ctrl {
public:
    Ctrl(Comm& comm) : comm(comm) {}
    ~Ctrl() {}

private:
    Comm& comm;

private:
    struct Sym {
        String name;

        Sym(String name);
        ~Sym() {}

        virtual String str() = 0;;
    };

    struct Dev : Sym {
        const uint8_t pin;

        Dev(const uint8_t pin, String name);
        ~Dev() {}
    };

protected:
    struct Read : Dev {
        Read(const uint8_t pin, String name);

        boolean get();
        String str();
    };
    struct Sens : Dev {
        Sens(const uint8_t pin, String name);

        uint16_t get();
        String str();
    };

    struct Flip : Dev {
        boolean _flip;
        boolean state;

        Flip(const uint8_t pin, String name, const boolean _flip = true);

        void set(const boolean state);
        boolean get();
        String str();
    };

    struct UNum : Sym {
        UNum(String name);

        String str();
    };

    struct UStr : Sym {
        UStr(String name);

        String get();
        String str();
    };

public:
    Flip report = Flip(REPORT_CONN, REPORT_NAME, false);
    Flip flip_1 = Flip(FLIP_1_CONN, FLIP_1_NAME);
    Flip flip_2 = Flip(FLIP_2_CONN, FLIP_2_NAME);
    Flip flip_3 = Flip(FLIP_3_CONN, FLIP_3_NAME);
    Flip flip_4 = Flip(FLIP_4_CONN, FLIP_4_NAME);
    Read read_1 = Read(READ_1_CONN, READ_1_NAME);
    Read read_2 = Read(READ_2_CONN, READ_2_NAME);
    Sens sens_1 = Sens(SENS_1_CONN, SENS_1_NAME);
    Sens sens_2 = Sens(SENS_2_CONN, SENS_2_NAME);
    UNum up_num = UNum(UP_NUM_NAME);
    UStr up_str = UStr(UP_STR_NAME);

public:
    template<typename T>
    String repr(T sym, const bool trailing = true) {
        String val = this->comm.join(
            this->comm.join("\"", sym.name, "\":"),
            sym.str()
        );
        return trailing ? this->comm.join(val, ",") : val;
    }
};

#endif
