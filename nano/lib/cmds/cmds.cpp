#include "cmds.hpp"


void Cmds::loop() {
    const char entry = this->comm.collect();
    switch (entry) {
        case CODE_BACKSP:   this->remove(false);    break;
        case CODE_ESCAPE:   this->remove(true);     break;
        case CODE_RETURN:   this->trigger();        break;
        default:            this->collect(entry);   break;
    }
}


void Cmds::collect(const char chr) {
    if (31 < chr && 127 > chr) {
        if (PROMPT_SIZE > this->input.length()) {
            this->input.concat(chr);
            this->comm.raw(chr);
        }
    }
}
void Cmds::remove(const bool all) {
    while (this->input.length()) {
        this->input.remove(this->input.length() -1, 1);
        this->comm.raw(CODE_BACKSP);
        if (!all) { return; }
    }
}

void Cmds::trigger() {
    this->input.trim();

    if (this->comm.contains(this->input, FLIP_1_FULL)) {
        this->ctrl.flip_1.set(true);
    }
    if (this->comm.contains(this->input, FLIP_1_NULL)) {
        this->ctrl.flip_1.set(false);
    }
    if (this->comm.contains(this->input, FLIP_2_FULL)) {
        this->ctrl.flip_2.set(true);
    }
    if (this->comm.contains(this->input, FLIP_2_NULL)) {
        this->ctrl.flip_2.set(false);
    }
    if (this->comm.contains(this->input, FLIP_3_FULL)) {
        this->ctrl.flip_3.set(true);
    }
    if (this->comm.contains(this->input, FLIP_3_NULL)) {
        this->ctrl.flip_3.set(false);
    }
    if (this->comm.contains(this->input, FLIP_4_FULL)) {
        this->ctrl.flip_4.set(true);
    }
    if (this->comm.contains(this->input, FLIP_4_NULL)) {
        this->ctrl.flip_4.set(false);
    }

    this->remove(true);
    this->comm.newline();
    this->report();
}

void Cmds::report() {
    this->comm.text("{");

    this->comm.text(this->ctrl.repr(this->ctrl.flip_1));
    this->comm.text(this->ctrl.repr(this->ctrl.flip_2));
    this->comm.text(this->ctrl.repr(this->ctrl.flip_3));
    this->comm.text(this->ctrl.repr(this->ctrl.flip_4));

    this->comm.text(this->ctrl.repr(this->ctrl.read_1));
    this->comm.text(this->ctrl.repr(this->ctrl.read_2));
    this->comm.text(this->ctrl.repr(this->ctrl.sens_1));
    this->comm.text(this->ctrl.repr(this->ctrl.sens_2));

    this->comm.text(this->ctrl.repr(this->ctrl.up_num));
    this->comm.text(this->ctrl.repr(this->ctrl.up_str, false));

    this->comm.text("}");
    this->comm.newline();
}
