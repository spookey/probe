#ifndef _cmds_hpp_
#define _cmds_hpp_

#include "init.hpp"
#include "comm.hpp"
#include "ctrl.hpp"

class Cmds {
public:
    Cmds(Comm& comm, Ctrl& ctrl) : comm(comm), ctrl(ctrl) {}
    ~Cmds() {}

public:
    void loop();

private:
    Comm& comm;
    Ctrl& ctrl;

    String input = String();

protected:
    void collect(const char chr);
    void remove(const bool all);
    void trigger();

    void report();

};

#endif
