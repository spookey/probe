#ifndef _init_hpp_
#define _init_hpp_

#include <Arduino.h>

#define IDENT_TITLE         "Probe"
#define IDENT_GREET         "v0.23"


#define CHAR_IGNORE         '\0'            // null
#define CODE_BACKSP         8               // backspace
#define CODE_ESCAPE         27              // escape
#define CODE_FILLER         32              // space
#define CODE_RETURN         13              // carriage return


#define SERIAL_CONF         SERIAL_8N1      // serial config
#ifndef SERIAL_BAUD
#define SERIAL_BAUD         115200          // serial baud rate
#endif
#define PROMPT_SIZE         16              // prompt buffer size

#ifndef REPORT_CONN
#define REPORT_CONN         LED_BUILTIN     // pin of status led
#endif
#ifndef REPORT_NAME
#define REPORT_NAME         "report"        // name of status led
#endif
#ifndef FLIP_1_CONN
#define FLIP_1_CONN         2               // pin to flip
#endif
#ifndef FLIP_1_NAME
#define FLIP_1_NAME         "flip1"         // name of flip
#endif
#ifndef FLIP_1_FULL
#define FLIP_1_FULL         "1f"           // full command of flip
#endif
#ifndef FLIP_1_NULL
#define FLIP_1_NULL         "1n"           // null command of flip
#endif
#ifndef FLIP_2_CONN
#define FLIP_2_CONN         3               // pin to flip
#endif
#ifndef FLIP_2_NAME
#define FLIP_2_NAME         "flip2"         // name of flip
#endif
#ifndef FLIP_2_FULL
#define FLIP_2_FULL         "2f"           // full command of flip
#endif
#ifndef FLIP_2_NULL
#define FLIP_2_NULL         "2n"           // null command of flip
#endif
#ifndef FLIP_3_CONN
#define FLIP_3_CONN         4               // pin to flip
#endif
#ifndef FLIP_3_NAME
#define FLIP_3_NAME         "flip3"         // name of flip
#endif
#ifndef FLIP_3_FULL
#define FLIP_3_FULL         "3f"           // full command of flip
#endif
#ifndef FLIP_3_NULL
#define FLIP_3_NULL         "3n"           // null command of flip
#endif
#ifndef FLIP_4_CONN
#define FLIP_4_CONN         5               // pin to flip
#endif
#ifndef FLIP_4_NAME
#define FLIP_4_NAME         "flip4"         // name of flip
#endif
#ifndef FLIP_4_FULL
#define FLIP_4_FULL         "4f"           // full command of flip
#endif
#ifndef FLIP_4_NULL
#define FLIP_4_NULL         "4n"           // null command of flip
#endif

#ifndef READ_1_CONN
#define READ_1_CONN         8               // pin to 1/0 read
#endif
#ifndef READ_1_NAME
#define READ_1_NAME         "read1"         // name of 1/0 read
#endif
#ifndef READ_2_CONN
#define READ_2_CONN         7               // pin to 1/0 read
#endif
#ifndef READ_2_NAME
#define READ_2_NAME         "read2"         // name of 1/0 read
#endif
#ifndef SENS_1_CONN
#define SENS_1_CONN         A1              // pin to float read
#endif
#ifndef SENS_1_NAME
#define SENS_1_NAME         "sens1"         // name of float read
#endif
#ifndef SENS_2_CONN
#define SENS_2_CONN         A2              // pin to float read
#endif
#ifndef SENS_2_NAME
#define SENS_2_NAME         "sens1"         // name of float read
#endif

#ifndef UP_NUM_NAME
#define UP_NUM_NAME         "upNum"         // name numeric uptime
#endif
#ifndef UP_STR_NAME
#define UP_STR_NAME         "upStr"         // name string uptime
#endif

#endif
