#include "comm.hpp"

void Comm::begin() {
    Serial.begin(SERIAL_BAUD, SERIAL_CONF);
    while (!Serial) { delay(0.01); }
    this->newline();
    this->message(this->join(IDENT_TITLE, " [", IDENT_GREET, "]"));
}


String Comm::join(String aa, String bb) {
    return String(aa + bb);
}
String Comm::join(String aa, String bb, String cc) {
    return String(aa + bb + cc);
}
String Comm::join(String aa, String bb, String cc, String dd) {
    return String(aa + bb + cc + dd);
}


bool Comm::contains(const String input, const String search) {
    return 0 <= input.indexOf(search);
}


void Comm::text(const String msg) {
    Serial.print(msg);
    Serial.flush();
}

void Comm::message(const String msg) {
    Serial.println(msg);
    Serial.flush();
}

void Comm::raw(const char chr) {
    Serial.write(chr);
}

const char Comm::collect() {
    const int32_t val = Serial.read();
    if (val >= 0) { return val; }
    return CHAR_IGNORE;
}
