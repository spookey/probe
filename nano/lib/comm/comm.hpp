#ifndef _comm_hpp_
#define _comm_hpp_

#include "init.hpp"

class Comm {
public:
    Comm() {}
    ~Comm() {}

public:
    void begin();

public:
    String join(String aa, String bb);
    String join(String aa, String bb, String cc);
    String join(String aa, String bb, String cc, String dd);

public:
    bool contains(const String input, const String search);

public:
    void text(const String msg);
    void message(const String msg);
    void newline() { this->message(""); }
    void raw(const char chr);
    const char collect();

};

#endif
