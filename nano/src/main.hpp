#ifndef _main_hpp_
#define _main_hpp_

#include "init.hpp"
#include "cmds.hpp"
#include "comm.hpp"
#include "ctrl.hpp"

void setup();
void loop();

#endif
