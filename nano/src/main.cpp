#include "main.hpp"

Comm comm = Comm();
Ctrl ctrl = Ctrl(comm);
Cmds cmds = Cmds(comm, ctrl);

void setup() {
    comm.begin();
}

void loop() {
    cmds.loop();
}
