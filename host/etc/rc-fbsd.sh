#!/bin/sh
#
# PROVIDE: probe
# REQUIRE: LOGIN mysql
# KEYWORD: shutdown
#

. /etc/rc.subr

name=probe
rcvar=probe_enable

load_rc_config ${name}

: ${probe_enable:="NO"}
: ${probe_user:="duke"}
: ${probe_group:="duke"}
: ${probe_base_folder:="/usr/home/${probe_user}/run/${name}"}
: ${probe_application_jar:="${probe_base_folder}/${name}.jar"}
: ${probe_application_conf:="file:${probe_base_folder}/application.yaml"}
: ${probe_application_profiles:=""}
: ${probe_logging_conf:="classpath:/logback-spring.xml"}
: ${probe_logging_file:="/var/log/${probe_user}/${name}.log"}

logfile="/var/log/${name}.log"
pidfile="/var/run/${name}.pid"

procname="/usr/local/openjdk21/bin/java"
command="/usr/sbin/daemon"
command_args="\
  -f -T ${name} -t ${name} -o ${logfile} -p ${pidfile} \
  ${procname} -server \
  -Djava.net.preferIPv4Stack=false \
  -Djava.net.preferIPv6Addresses=true \
  -Dlogging.file.name=${probe_logging_file} \
  -Dlogging.config=${probe_logging_conf} \
  -Dspring.profiles.active=${probe_application_profiles} \
  -jar ${probe_application_jar} \
  --spring.config.additional-location=${probe_application_conf} \
"

extra_commands="reload"
start_precmd="probe_start_precmd"

probe_start_precmd() {
  install -o ${probe_user} -g ${probe_group} -m 644 -- /dev/null ${logfile}
  install -o ${probe_user} -g ${probe_group} -m 600 -- /dev/null ${pidfile}
}

run_rc_command "$1"
