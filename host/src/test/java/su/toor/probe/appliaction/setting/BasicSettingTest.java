package su.toor.probe.appliaction.setting;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZoneId;
import org.junit.jupiter.api.Test;

class BasicSettingTest {

    private static void verify(
            final BasicSetting setting,
            final Boolean expectedAllowUninitializedPort,
            final Boolean expectedTeardownOnDisconnect,
            final ZoneId expectedTimezone) {
        assertThat(setting.getAllowUninitializedPort()).isEqualTo(expectedAllowUninitializedPort);
        assertThat(setting.getTeardownOnPortDisconnect()).isEqualTo(expectedTeardownOnDisconnect);
        assertThat(setting.getTimezone()).isEqualTo(expectedTimezone);
    }

    private static void verifyEmpty(final BasicSetting setting) {
        verify(
                setting,
                BasicSetting.DEFAULT_ALLOW_UNINITIALIZED_PORT,
                BasicSetting.DEFAULT_TEARDOWN_ON_PORT_DISCONNECT,
                BasicSetting.DEFAULT_TIMEZONE);
    }

    @Test
    void empty() {
        final var setting = new BasicSetting();

        verifyEmpty(setting);
    }

    @Test
    void nulls() {
        final var setting = new BasicSetting()
                .setAllowUninitializedPort(null)
                .setTeardownOnPortDisconnect(null)
                .setTimezone(null);

        verifyEmpty(setting);
    }

    @Test
    void some() {
        final var allowUninitializedPort = true;
        final var teardownOnPortDisconnect = false;
        final var timezoneName = "Asia/Tokyo";
        final var timezone = ZoneId.of(timezoneName);

        final var setting = new BasicSetting()
                .setAllowUninitializedPort(allowUninitializedPort)
                .setTeardownOnPortDisconnect(teardownOnPortDisconnect)
                .setTimezone(timezone);

        verify(setting, allowUninitializedPort, teardownOnPortDisconnect, timezone);

        assertThat(setting.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(String.valueOf(allowUninitializedPort))
                .contains(String.valueOf(teardownOnPortDisconnect))
                .contains(timezoneName);
    }
}
