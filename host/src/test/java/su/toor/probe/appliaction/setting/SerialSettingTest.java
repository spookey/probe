package su.toor.probe.appliaction.setting;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import su.toor.probe.appliaction.setting.serial.SerialDataBits;
import su.toor.probe.appliaction.setting.serial.SerialParity;
import su.toor.probe.appliaction.setting.serial.SerialStopBits;

class SerialSettingTest {

    private static void verify(
            final SerialSetting setting,
            final String expectedPort,
            final Integer expectedBaud,
            final SerialDataBits expectedDataBits,
            final SerialParity expectedParity,
            final SerialStopBits expectedStopBits) {
        assertThat(setting.getPort()).isEqualTo(expectedPort);
        assertThat(setting.getBaud()).isEqualTo(expectedBaud);
        assertThat(setting.getDataBits()).isEqualTo(expectedDataBits);
        assertThat(setting.getParity()).isEqualTo(expectedParity);
        assertThat(setting.getStopBits()).isEqualTo(expectedStopBits);
    }

    private static void verifyEmpty(final SerialSetting setting) {
        verify(
                setting,
                null,
                SerialSetting.DEFAULT_BAUD,
                SerialSetting.DEFAULT_DATA_BITS,
                SerialSetting.DEFAULT_PARITY,
                SerialSetting.DEFAULT_STOP_BITS);
    }

    @Test
    void empty() {
        verifyEmpty(new SerialSetting());
        verifyEmpty(new SerialSetting().setPort(""));
        verifyEmpty(new SerialSetting().setPort("\n"));

        verifyEmpty(new SerialSetting()
                .setPort(null)
                .setBaud(null)
                .setDataBits(null)
                .setParity(null)
                .setStopBits(null));
    }

    @Test
    void some() {
        final var port = "/dev/tty.does-not-exist";
        final var baud = 9600;
        final var dataBits = SerialDataBits.FIVE;
        final var parity = SerialParity.ODD;
        final var stopBits = SerialStopBits.ONE_POINT_FIVE;

        final var setting = new SerialSetting()
                .setPort(port)
                .setBaud(baud)
                .setDataBits(dataBits)
                .setParity(parity)
                .setStopBits(stopBits);

        verify(setting, port, baud, dataBits, parity, stopBits);

        assertThat(setting.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(port)
                .contains(String.valueOf(baud))
                .contains(dataBits.name())
                .contains(parity.name())
                .contains(stopBits.name());
    }
}
