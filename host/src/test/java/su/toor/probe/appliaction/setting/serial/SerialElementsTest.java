package su.toor.probe.appliaction.setting.serial;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class SerialElementsTest {

    private static void verifyValue(final Serial serial) {
        assertThat(serial.getValue()).isNotNegative();
    }

    @ParameterizedTest
    @EnumSource(SerialDataBits.class)
    void dataBits(final SerialDataBits serialDataBits) {
        verifyValue(serialDataBits);
    }

    @ParameterizedTest
    @EnumSource(SerialParity.class)
    void parity(final SerialParity serialParity) {
        verifyValue(serialParity);
    }

    @ParameterizedTest
    @EnumSource
    void stopBits(final SerialStopBits serialStopBits) {
        verifyValue(serialStopBits);
    }
}
