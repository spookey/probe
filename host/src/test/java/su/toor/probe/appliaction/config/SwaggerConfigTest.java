package su.toor.probe.appliaction.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import su.toor.probe.FakeBean;

class SwaggerConfigTest {

    private static final FakeBean FAKE_BEAN = new FakeBean();
    private static final SwaggerConfig SWAGGER_CONFIG = new SwaggerConfig();

    @Test
    void openApi() {
        final var buildProperties = FAKE_BEAN.buildProperties();
        final var openAPI = SWAGGER_CONFIG.openAPI(buildProperties);
        assertThat(openAPI).isNotNull();

        final var info = openAPI.getInfo();
        assertThat(info).isNotNull();

        assertThat(info.getTitle()).isEqualTo(FakeBean.APPLICATION_NAME);
        assertThat(info.getVersion()).isEqualTo(FakeBean.APPLICATION_VERSION);
        assertThat(info.getDescription()).isEqualTo(FakeBean.APPLICATION_DESCRIPTION);
    }
}
