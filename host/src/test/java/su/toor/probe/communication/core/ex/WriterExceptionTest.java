package su.toor.probe.communication.core.ex;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import org.junit.jupiter.api.Test;

class WriterExceptionTest {

    @Test
    void writerException() {
        final var cause = mock(IOException.class);
        final var ex = new WriterException(cause);

        assertThat(ex.getCause()).isSameAs(cause);
    }
}
