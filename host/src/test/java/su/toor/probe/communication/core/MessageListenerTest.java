package su.toor.probe.communication.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import su.toor.probe.shared.StateDto;

class MessageListenerTest {

    @Test
    void settings() {
        final var communicator = mock(Communication.class);

        final var listener = new MessageListener(communicator);

        assertThat(listener.getListeningEvents())
                .isEqualTo(SerialPort.LISTENING_EVENT_PORT_DISCONNECTED ^ SerialPort.LISTENING_EVENT_DATA_RECEIVED)
                .isEqualTo(0x10000010);
        assertThat(listener.getMessageDelimiter()).isEqualTo(new byte[] {0x0d, 0x0a});
        assertThat(listener.delimiterIndicatesEndOfMessage()).isTrue();

        verifyNoInteractions(communicator);
    }

    @Test
    void invalidEvents() {
        final var event = mock(SerialPortEvent.class);
        final var communicator = mock(Communication.class);

        Stream.of(
                        SerialPort.LISTENING_EVENT_TIMED_OUT,
                        SerialPort.LISTENING_EVENT_DATA_AVAILABLE,
                        SerialPort.LISTENING_EVENT_DATA_WRITTEN,
                        SerialPort.LISTENING_EVENT_BREAK_INTERRUPT,
                        SerialPort.LISTENING_EVENT_CARRIER_DETECT,
                        SerialPort.LISTENING_EVENT_CTS,
                        SerialPort.LISTENING_EVENT_DSR,
                        SerialPort.LISTENING_EVENT_RING_INDICATOR,
                        SerialPort.LISTENING_EVENT_FRAMING_ERROR,
                        SerialPort.LISTENING_EVENT_FIRMWARE_OVERRUN_ERROR,
                        SerialPort.LISTENING_EVENT_SOFTWARE_OVERRUN_ERROR,
                        SerialPort.LISTENING_EVENT_PARITY_ERROR)
                .forEach(eventType -> {
                    when(event.getEventType()).thenReturn(eventType);

                    final var listener = new MessageListener(communicator);
                    listener.serialEvent(event);

                    verify(event, times(1)).getEventType();
                    verifyNoMoreInteractions(event);
                    reset(event);

                    verifyNoInteractions(communicator);
                    reset(communicator);
                });
    }

    @Test
    void disconnected() {
        final var event = mock(SerialPortEvent.class);
        final var communicator = mock(Communication.class);

        when(event.getEventType()).thenReturn(SerialPort.LISTENING_EVENT_PORT_DISCONNECTED);

        final var listener = new MessageListener(communicator);
        listener.serialEvent(event);

        verify(event, times(1)).getEventType();
        verifyNoMoreInteractions(event);

        verify(communicator, times(1)).teardown();
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void invalidJson() {
        final var event = mock(SerialPortEvent.class);
        final var communicator = mock(Communication.class);

        when(event.getEventType()).thenReturn(SerialPort.LISTENING_EVENT_DATA_RECEIVED);
        when(event.getReceivedData()).thenReturn("[invalid message]".getBytes(StandardCharsets.UTF_8));

        final var listener = new MessageListener(communicator);

        listener.serialEvent(event);

        verify(event, times(1)).getEventType();
        verify(event, times(1)).getReceivedData();
        verifyNoMoreInteractions(event);

        verifyNoInteractions(communicator);
    }

    @Test
    void validJson() throws IOException {
        final var sens1 = 23;
        final var sens2 = 42;
        final var upNum = 1337L;
        final var upStr = "yes!";

        final var stateDto = new StateDto()
                .setFlip1(true)
                .setFlip2(true)
                .setFlip3(true)
                .setFlip4(true)
                .setRead1(true)
                .setRead2(true)
                .setSens1(sens1)
                .setSens2(sens2)
                .setUpNum(upNum)
                .setUpStr(upStr);

        final var payload = new ObjectMapper().writeValueAsBytes(stateDto);

        final var event = mock(SerialPortEvent.class);
        final var communicator = mock(Communication.class);

        when(event.getEventType()).thenReturn(SerialPort.LISTENING_EVENT_DATA_RECEIVED);
        when(event.getReceivedData()).thenReturn(payload);

        final var listener = new MessageListener(communicator);
        listener.serialEvent(event);

        verify(event, times(1)).getEventType();
        verify(event, times(1)).getReceivedData();
        verifyNoMoreInteractions(event);

        verify(communicator, times(1)).setState(stateDto);
        verifyNoMoreInteractions(communicator);
    }
}
