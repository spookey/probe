package su.toor.probe.communication.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanInstantiationException;
import su.toor.probe.appliaction.setting.SerialSetting;

class PortComponentTest {

    @Test
    void emptyConfig() {
        final var setting = new SerialSetting();

        final var component = new PortComponent();
        assertThatExceptionOfType(BeanInstantiationException.class)
                .isThrownBy(() -> component.createSerialPort(setting))
                .withMessageContaining("not configured");
    }

    @Test
    void invalidPort() {
        final var port = "/dev/tty.does-not-exist";
        final var setting = new SerialSetting().setPort(port);

        final var component = new PortComponent();
        assertThatExceptionOfType(BeanInstantiationException.class)
                .isThrownBy(() -> component.createSerialPort(setting))
                .withMessageContaining("cannot connect");
    }

    @Test
    void someConfig() {
        final var port = "/dev/null";
        final var setting = new SerialSetting().setPort(port);

        final var component = new PortComponent();
        final var serialPort = component.createSerialPort(setting);

        assertThat(serialPort).isNotNull();
        assertThat(serialPort.getSystemPortPath()).isEqualTo(port);
        assertThat(serialPort.getBaudRate()).isEqualTo(setting.getBaud());
        assertThat(serialPort.getNumDataBits()).isEqualTo(setting.getDataBits().getValue());
        assertThat(serialPort.getParity()).isEqualTo(setting.getParity().getValue());
        assertThat(serialPort.getNumStopBits()).isEqualTo(setting.getStopBits().getValue());

        assertThat(serialPort.isOpen()).isFalse();
    }
}
