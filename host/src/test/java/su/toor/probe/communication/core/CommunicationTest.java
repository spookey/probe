package su.toor.probe.communication.core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class CommunicationTest {

    @Test
    void delimiter() {
        assertThat(Communication.CRLF).isEqualTo("\r\n");

        assertThat(Communication.asBytes(Communication.CRLF)).isEqualTo(new byte[] {0x0d, 0x0a});
    }

    @ParameterizedTest
    @ValueSource(strings = {"🔥", "\uD83D\uDD25"})
    void asBytes(final String text) {
        assertThat(Communication.asBytes(text)).isEqualTo(new byte[] {-0x10, -0x61, -0x6c, -0x5b});
    }
}
