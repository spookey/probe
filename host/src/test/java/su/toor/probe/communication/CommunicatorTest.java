package su.toor.probe.communication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.fazecast.jSerialComm.SerialPort;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.appliaction.setting.BasicSetting;
import su.toor.probe.communication.core.Communication;
import su.toor.probe.communication.core.MessageListener;
import su.toor.probe.communication.core.ex.UninitializedException;
import su.toor.probe.communication.core.ex.WriterException;
import su.toor.probe.component.TeardownComponent.TeardownHandler;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.StateDto;

@ExtendWith(SpringExtension.class)
class CommunicatorTest {

    private static final String SERIAL_SYSTEM_PORT_PATH = "/var/log/messages";
    private static final String INITIAL_MESSAGE = Flip.UPDATE.getMessage(false);

    @MockitoBean
    private BasicSetting basicSetting;

    @MockitoBean
    private SerialPort serialPort;

    @MockitoBean
    private TeardownHandler teardownHandler;

    @MockitoBean
    private AtomicBoolean initialized;

    private Communicator create() {
        return new Communicator(basicSetting, serialPort, teardownHandler, initialized);
    }

    @Test
    void initialized() {
        final var component = Communicator.atomicBoolean();
        assertThat(component.get()).isFalse();
    }

    @Test
    void creator() throws UninitializedException {
        try (final var communicator = mockConstruction(Communicator.class, (mock, ctx) -> {
            when(mock.open()).thenReturn(true);
            when(mock.register()).thenReturn(true);
        })) {
            final var component = Communicator.communicator(basicSetting, serialPort, teardownHandler, initialized);

            final var constructed = communicator.constructed().getFirst();
            assertThat(component).isSameAs(constructed);
        }
    }

    @Test
    void creatorOpenFailed() {
        try (final var ignored = mockConstruction(
                Communicator.class, (mock, ctx) -> when(mock.open()).thenReturn(false))) {
            assertThatExceptionOfType(BeanInstantiationException.class)
                    .isThrownBy(() -> Communicator.communicator(basicSetting, serialPort, teardownHandler, initialized))
                    .withMessageContaining("connection failed");
        }
    }

    @Test
    void creatorInitializeFailed() {
        try (final var ignored = mockConstruction(Communicator.class, (mock, ctx) -> {
            when(mock.open()).thenReturn(true);
            when(mock.register()).thenReturn(false);
        })) {
            assertThatExceptionOfType(BeanInstantiationException.class)
                    .isThrownBy(() -> Communicator.communicator(basicSetting, serialPort, teardownHandler, initialized))
                    .withMessageContaining("registration failed");
        }
    }

    @Test
    void openInitialized() {
        when(initialized.get()).thenReturn(true);

        final var comm = create();
        assertThat(comm.open()).isFalse();

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);
    }

    @Test
    void openIssues() {
        when(initialized.get()).thenReturn(false);
        when(serialPort.openPort()).thenReturn(false);
        when(basicSetting.getAllowUninitializedPort()).thenReturn(false);

        final var comm = create();
        assertThat(comm.open()).isFalse();

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).openPort();
        verifyNoMoreInteractions(serialPort);

        verify(basicSetting, times(1)).getAllowUninitializedPort();
        verifyNoMoreInteractions(basicSetting);
    }

    @Test
    void openHasIssuesButIsAllowed() {
        when(initialized.get()).thenReturn(false);
        when(serialPort.openPort()).thenReturn(false);
        when(basicSetting.getAllowUninitializedPort()).thenReturn(true);

        final var comm = create();
        assertThat(comm.open()).isTrue();

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).openPort();
        verifyNoMoreInteractions(serialPort);

        verify(basicSetting, times(1)).getAllowUninitializedPort();
        verifyNoMoreInteractions(basicSetting);
    }

    @Test
    void openSuccess() {
        when(initialized.get()).thenReturn(false).thenReturn(true);
        when(serialPort.openPort()).thenReturn(true);
        when(serialPort.isOpen()).thenReturn(true);

        final var comm = create();
        assertThat(comm.open()).isTrue();

        verify(initialized, times(2)).get();
        verify(initialized, times(1)).set(true);
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).openPort();
        verify(serialPort, times(1)).isOpen();
        verifyNoMoreInteractions(serialPort);
    }

    @Test
    void registerUninitialized() throws UninitializedException {
        when(initialized.get()).thenReturn(false).thenReturn(false);

        when(basicSetting.getAllowUninitializedPort()).thenReturn(true).thenReturn(false);

        final var comm = create();
        assertThat(comm.register()).isTrue();
        assertThat(comm.register()).isFalse();

        verify(initialized, times(2)).get();
        verifyNoMoreInteractions(initialized);

        verify(basicSetting, times(2)).getAllowUninitializedPort();
        verifyNoMoreInteractions(basicSetting);
    }

    @Test
    void registerListenerIssues() throws UninitializedException {
        when(initialized.get()).thenReturn(true);
        when(serialPort.addDataListener(any(MessageListener.class))).thenReturn(false);

        final var comm = create();
        assertThat(comm.register()).isFalse();

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).addDataListener(any(MessageListener.class));
        verifyNoMoreInteractions(serialPort);
    }

    @Test
    void registerMessageIssues() throws UninitializedException, WriterException {
        when(initialized.get()).thenReturn(true);
        when(serialPort.addDataListener(any(MessageListener.class))).thenReturn(true);

        final var comm = spy(create());
        doThrow(new UninitializedException(SERIAL_SYSTEM_PORT_PATH)).when(comm).message(INITIAL_MESSAGE);

        assertThat(comm.register()).isFalse();

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).addDataListener(any(MessageListener.class));
        verifyNoMoreInteractions(serialPort);

        verify(comm, times(1)).register();
        verify(comm, times(1)).message(INITIAL_MESSAGE);
        verifyNoMoreInteractions(comm);
    }

    @Test
    void register() throws UninitializedException, WriterException {
        when(initialized.get()).thenReturn(true).thenReturn(true);
        when(serialPort.addDataListener(any(MessageListener.class))).thenReturn(true);

        final var comm = spy(create());
        doNothing().when(comm).message(INITIAL_MESSAGE);

        assertThat(comm.register()).isTrue();

        verify(initialized, times(2)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).addDataListener(any(MessageListener.class));
        verifyNoMoreInteractions(serialPort);

        verify(comm, times(1)).register();
        verify(comm, times(1)).message(INITIAL_MESSAGE);
        verifyNoMoreInteractions(comm);
    }

    @Test
    void teardownIgnored() {
        when(basicSetting.getTeardownOnPortDisconnect()).thenReturn(false);

        final var comm = create();
        assertThatNoException().isThrownBy(comm::teardown);

        verify(initialized, times(1)).set(false);
        verifyNoMoreInteractions(initialized);

        verify(basicSetting, times(1)).getTeardownOnPortDisconnect();
        verifyNoMoreInteractions(basicSetting);

        verifyNoInteractions(teardownHandler);
    }

    @Test
    void teardown() {
        when(basicSetting.getTeardownOnPortDisconnect()).thenReturn(true);

        final var comm = create();
        assertThatNoException().isThrownBy(comm::teardown);

        verify(initialized, times(1)).set(false);
        verifyNoMoreInteractions(initialized);

        verify(basicSetting, times(1)).getTeardownOnPortDisconnect();
        verifyNoMoreInteractions(basicSetting);

        verify(teardownHandler, times(1)).applicationQuit(Communicator.PORT_DISCONNECT_EXIT_CODE);
        verifyNoMoreInteractions(teardownHandler);
    }

    @Test
    void stateExchange() {
        final var comm = create();

        final var empty = new StateDto();
        final var filled = new StateDto()
                .setFlip1(true)
                .setFlip2(false)
                .setFlip3(true)
                .setFlip4(false)
                .setRead1(true)
                .setRead1(false)
                .setSens1(13)
                .setSens2(12)
                .setUpNum(0L)
                .setUpStr("0");

        final var initial = comm.getState();
        assertThat(initial).isEqualTo(empty);

        comm.setState(filled);
        assertThat(comm.getState()).isSameAs(initial).isEqualTo(filled);

        comm.setState(empty);
        assertThat(comm.getState()).isSameAs(initial).isEqualTo(empty);
    }

    @Test
    void messageUninitialized() {
        final var message = "some test message";

        when(initialized.get()).thenReturn(false);
        when(serialPort.getSystemPortPath()).thenReturn(SERIAL_SYSTEM_PORT_PATH);

        final var comm = create();
        assertThatExceptionOfType(UninitializedException.class).isThrownBy(() -> comm.message(message));

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).getSystemPortPath();
        verifyNoMoreInteractions(serialPort);
    }

    @Test
    void messageIssues() throws IOException {
        final var message = "some test message";
        final var errorMessage = "some weird error message";

        final var stream = mock(OutputStream.class);
        doThrow(new IOException(errorMessage)).when(stream).write(any());

        when(initialized.get()).thenReturn(true);
        when(serialPort.getOutputStream()).thenReturn(stream);

        final var comm = create();
        assertThatExceptionOfType(WriterException.class)
                .isThrownBy(() -> comm.message(message))
                .withMessageContaining(errorMessage);

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).getOutputStream();
        verifyNoMoreInteractions(serialPort);

        verify(stream, times(1)).write(Communication.asBytes(message));
        verify(stream, times(1)).close();
        verifyNoMoreInteractions(stream);
    }

    @Test
    void message() throws IOException {
        final var message = "some test message";

        final var stream = mock(OutputStream.class);
        doNothing().when(stream).write(any());

        when(initialized.get()).thenReturn(true);
        when(serialPort.getOutputStream()).thenReturn(stream);

        final var comm = create();
        assertThatNoException().isThrownBy(() -> comm.message(message));

        verify(initialized, times(1)).get();
        verifyNoMoreInteractions(initialized);

        verify(serialPort, times(1)).getOutputStream();
        verifyNoMoreInteractions(serialPort);

        verify(stream, times(1)).write(Communication.asBytes(message));
        verify(stream, times(1)).write(Communication.asBytes(Communication.CRLF));
        verify(stream, times(1)).close();
        verifyNoMoreInteractions(stream);
    }
}
