package su.toor.probe.communication.core.ex;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

class UninitializedExceptionTest {

    @Test
    void constants() {
        assertThat(UninitializedException.UNKNOWN).isEqualTo("unknown");
    }

    @ParameterizedTest
    @ValueSource(strings = "/some/port")
    @NullSource
    void uninitializedException(final String systemPortPath) {
        final var expect = Optional.ofNullable(systemPortPath).orElse(UninitializedException.UNKNOWN);

        final var ex = new UninitializedException(systemPortPath);
        assertThat(ex).hasMessageContainingAll("closed port", expect);
    }
}
