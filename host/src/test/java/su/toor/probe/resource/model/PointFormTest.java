package su.toor.probe.resource.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;
import su.toor.probe.service.EntityService;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.Target;

class PointFormTest {

    @Test
    void fields() {
        final var day = DayOfWeek.WEDNESDAY;
        final var hour = 23;
        final var minute = 42;
        final var target = Target.OFF;

        final var form = new PointForm(day, hour, minute, target);

        assertThat(form.day()).isEqualTo(day);
        assertThat(form.hour()).isEqualTo(hour);
        assertThat(form.minute()).isEqualTo(minute);
        assertThat(form.target()).isEqualTo(target);
    }

    @Test
    void from() {
        final var pointEntity = PointEntity.with(FlipEntity.with(Flip.FLIP_3, null), DayOfWeek.MONDAY, 13, 12, true);

        final var form = PointForm.from(pointEntity);

        assertThat(form.day()).isEqualTo(pointEntity.getDay());
        assertThat(form.hour()).isEqualTo(pointEntity.getHour());
        assertThat(form.minute()).isEqualTo(pointEntity.getMinute());
        assertThat(form.target()).isEqualTo(Target.resolve(pointEntity.getTarget()));
    }

    @Test
    void empty() {
        final var form = PointForm.empty();

        assertThat(form.day()).isEqualTo(EntityService.INITIAL_DAY);
        assertThat(form.hour()).isEqualTo(EntityService.INITIAL_HOUR);
        assertThat(form.minute()).isEqualTo(EntityService.INITIAL_MINUTE);
        assertThat(form.target()).isEqualTo(Target.resolve(EntityService.INITIAL_TARGET));
    }

    @Test
    void days() {
        final var days = PointForm.days();

        final var expectedDays = EnumSet.allOf(DayOfWeek.class).stream()
                .sorted(Comparator.comparingInt(DayOfWeek::ordinal))
                .toList();
        final var expectedNames = expectedDays.stream()
                .map(day -> day.getDisplayName(TextStyle.FULL, Locale.ENGLISH))
                .toList();

        final var elems = days.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedDays);

        final var names = days.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }

    @Test
    void hours() {
        final var hours = PointForm.hours();

        final var expectedElems = IntStream.rangeClosed(0, 23).boxed().toList();
        final var expectedNames = expectedElems.stream().map("%02d"::formatted).toList();

        final var elems = hours.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedElems);

        final var names = hours.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }

    @Test
    void minutes() {
        final var minutes = PointForm.minutes();

        final var expectedElems = IntStream.rangeClosed(0, 59).boxed().toList();
        final var expectedNames = expectedElems.stream().map("%02d"::formatted).toList();

        final var elems = minutes.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedElems);

        final var names = minutes.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }

    @Test
    void targets() {
        final var targets = PointForm.targets();

        final var expectedTargets = List.of(Target.OFF, Target.ON);
        final var expectedNames = expectedTargets.stream().map(Target::getLabel).toList();

        final var elems = targets.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedTargets);

        final var names = targets.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }
}
