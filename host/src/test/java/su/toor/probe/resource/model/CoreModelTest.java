package su.toor.probe.resource.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Locale;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.Target;

class CoreModelTest {

    @Test
    void constants() {
        assertThat(CoreModel.LOCALE).isEqualTo(Locale.ENGLISH);
        assertThat(CoreModel.UNICODE).isEqualTo(StandardCharsets.UTF_8);
    }

    @Test
    void with() {
        final var applicationName = "test-application";
        final var model = CoreModel.with(applicationName);

        assertThat(model.charset()).isEqualTo("utf-8");
        assertThat(model.language()).isEqualTo("en");
        assertThat(model.applicationName()).isEqualTo(applicationName);
    }

    @ParameterizedTest
    @EnumSource(Target.class)
    void formatState(final Target target) {
        assertThat(CoreModel.formatState(target.getState())).isEqualTo(target.getLabel());
        assertThat(CoreModel.formatState(target)).isEqualTo(target.getLabel());
    }

    @ParameterizedTest
    @EnumSource(DayOfWeek.class)
    void formatDay(final DayOfWeek dayOfWeek) {
        assertThat(CoreModel.formatDay(dayOfWeek)).isEqualTo(dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH));
    }

    @Test
    void formatNum() {
        IntStream.rangeClosed(0, 9)
                .forEach(num -> assertThat(CoreModel.formatNum(num)).isEqualTo("0" + num));
        assertThat(CoreModel.formatNum(10)).isEqualTo("10");
        assertThat(CoreModel.formatNum(99)).isEqualTo("99");
        assertThat(CoreModel.formatNum(100)).isEqualTo("100");
        assertThat(CoreModel.formatNum(999)).isEqualTo("999");
    }

    @Test
    void flips() {
        final var flips = CoreModel.flips();

        final var expectedElems = EnumSet.allOf(Flip.class).stream()
                .filter(flip -> !flip.isInternal())
                .sorted(Comparator.comparingInt(Flip::ordinal))
                .toList();
        final var expectedNames = expectedElems.stream().map(Flip::getLabel).toList();

        final var elems = flips.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedElems);

        final var names = flips.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }
}
