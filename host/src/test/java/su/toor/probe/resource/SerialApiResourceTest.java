package su.toor.probe.resource;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.probe.service.SerialService;
import su.toor.probe.shared.StateDto;

@WebMvcTest(SerialApiResource.class)
class SerialApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private SerialService serialService;

    private static URI stateUri() {
        return UriComponentsBuilder.fromPath("/api/serial/state").build().toUri();
    }

    @Test
    void getState() throws Exception {
        final var state = new StateDto()
                .setFlip1(false)
                .setFlip2(true)
                .setFlip3(false)
                .setFlip4(true)
                .setRead1(false)
                .setRead2(true)
                .setSens1(13)
                .setSens2(12)
                .setUpNum(1337L)
                .setUpStr("very much");

        when(serialService.getState()).thenReturn(state);

        mockMvc.perform(get(stateUri()).accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.flip1", equalTo(state.getFlip1())))
                .andExpect(jsonPath("$.flip2", equalTo(state.getFlip2())))
                .andExpect(jsonPath("$.flip3", equalTo(state.getFlip3())))
                .andExpect(jsonPath("$.flip4", equalTo(state.getFlip4())))
                .andExpect(jsonPath("$.read1", equalTo(state.getRead1())))
                .andExpect(jsonPath("$.read2", equalTo(state.getRead2())))
                .andExpect(jsonPath("$.sens1", equalTo(state.getSens1())))
                .andExpect(jsonPath("$.sens2", equalTo(state.getSens2())))
                .andExpect(jsonPath("$.upNum", equalTo(Math.toIntExact(state.getUpNum()))))
                .andExpect(jsonPath("$.upStr", equalTo(state.getUpStr())));

        verify(serialService, times(1)).getState();
        verifyNoMoreInteractions(serialService);
    }
}
