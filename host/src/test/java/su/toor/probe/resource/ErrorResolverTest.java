package su.toor.probe.resource;

import static org.assertj.core.api.Assertions.assertThat;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import su.toor.probe.FakeBean;
import su.toor.probe.resource.model.CoreModel;

@WebMvcTest(ErrorResolver.class)
@Import(FakeBean.class)
class ErrorResolverTest {

    @MockitoBean
    private HttpServletRequest httpServletRequest;

    @Autowired
    private ErrorResolver errorResolver;

    @Test
    void resolve() {
        final var httpStatus = HttpStatus.BAD_REQUEST;
        final var existingModelKey = "something";
        final var existingModelValue = new Object();

        final var immutableModel = Map.of(existingModelKey, existingModelValue);

        final var result = errorResolver.resolveErrorView(httpServletRequest, httpStatus, immutableModel);

        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo("error");
        assertThat(result.getStatus()).isEqualTo(httpStatus);

        final var model = result.getModel();
        assertThat(model).isNotNull().isNotEmpty();

        assertThat(model.getOrDefault(existingModelKey, null)).isNotNull().isEqualTo(existingModelValue);

        final var coreObject = model.getOrDefault("core", null);
        assertThat(coreObject).isNotNull().isInstanceOf(CoreModel.class);
    }
}
