package su.toor.probe.resource.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class NamedElemTest {

    @Test
    void direct() {
        final var elem = new Object();
        final var name = "object-name";

        final var named = new NamedElem<>(elem, name);

        assertThat(named.elem()).isEqualTo(elem);
        assertThat(named.name()).isEqualTo(name);
    }

    @Test
    void from() {
        final var result = NamedElem.from(IntStream.rangeClosed(1, 23).boxed(), num -> Integer.toString(num));

        final var pos = new AtomicInteger(0);
        result.forEach(res -> {
            assertThat(res.elem()).isEqualTo(pos.incrementAndGet());
            assertThat(res.name()).isEqualTo("%d".formatted(pos.get()));
        });
    }
}
