package su.toor.probe.resource.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.Target;

class FlipFormTest {

    @Test
    void fields() {
        final var label = "awesome-label";
        final var forced = Target.ON;

        final var form = new FlipForm(label, forced);

        assertThat(form.label()).isEqualTo(label);
        assertThat(form.forced()).isEqualTo(forced);
    }

    @Test
    void from() {
        final var flipEntity = FlipEntity.with(Flip.FLIP_4, "label", false);
        final var form = FlipForm.from(flipEntity);

        assertThat(form.label()).isEqualTo(flipEntity.getLabel());
        assertThat(form.forced()).isEqualTo(Target.resolve(flipEntity.getForced()));
    }

    @Test
    void targets() {
        final var targets = FlipForm.targets();

        final var expectedTargets = List.of(Target.TIMED, Target.OFF, Target.ON);
        final var expectedNames = expectedTargets.stream().map(Target::getLabel).toList();

        final var elems = targets.stream().map(NamedElem::elem).toList();
        assertThat(elems).containsExactlyElementsOf(expectedTargets);

        final var names = targets.stream().map(NamedElem::name).toList();
        assertThat(names).containsExactlyElementsOf(expectedNames);
    }
}
