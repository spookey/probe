package su.toor.probe.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;
import su.toor.probe.service.EntityService;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.FlipDto;
import su.toor.probe.shared.PointDto;

@WebMvcTest(GlanceApiResource.class)
class GlanceApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private EntityService entityService;

    @Autowired
    private ObjectMapper objectMapper;

    private static URI glanceUri() {
        return UriComponentsBuilder.fromPath("/api/glance/all").build().toUri();
    }

    @Test
    void getGlanceCreates() throws Exception {
        final var flip = Flip.UPDATE;
        final var flipEntity = FlipEntity.with(flip, flip.getLabel());
        final var pointEntity = PointEntity.with(flipEntity, DayOfWeek.FRIDAY, 23, 42, true);

        when(entityService.getOrCreateFlipEntities()).thenReturn(List.of(flipEntity));
        when(entityService.getOrCreatePointEntities(flipEntity)).thenReturn(List.of(pointEntity));

        mockMvc.perform(get(glanceUri()).accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", allOf(isA(List.class), hasSize(1))))
                .andExpect(jsonPath("$.[0]", isA(Map.class)))
                .andExpect(jsonPath("$.[0].flip", equalTo(flip.name())))
                .andExpect(jsonPath("$.[0].forced", equalTo(null)))
                .andExpect(jsonPath("$.[0].label", equalTo(flip.getLabel())))
                .andExpect(jsonPath("$.[0].points", allOf(isA(List.class), hasSize(1))))
                .andExpect(jsonPath("$.[0].points.[0]", isA(Map.class)))
                .andExpect(jsonPath(
                        "$.[0].points.[0].day", equalTo(pointEntity.getDay().name())))
                .andExpect(jsonPath("$.[0].points.[0].hour", equalTo(pointEntity.getHour())))
                .andExpect(jsonPath("$.[0].points.[0].minute", equalTo(pointEntity.getMinute())))
                .andExpect(jsonPath("$.[0].points.[0].target", equalTo(pointEntity.getTarget())));

        verify(entityService, times(1)).getOrCreateFlipEntities();
        verify(entityService, times(1)).getOrCreatePointEntities(flipEntity);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void putGlanceEmpty() throws Exception {
        final var payload = objectMapper.writeValueAsString(List.of());

        mockMvc.perform(put(glanceUri()).contentType(APPLICATION_JSON).content(payload))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl(glanceUri().toString()));

        verifyNoInteractions(entityService);
    }

    @Test
    void putGlancePointModify() throws Exception {
        final var flip = Flip.FLIP_1;
        final var label = "some label";
        final var forced = false;

        final var day = DayOfWeek.WEDNESDAY;
        final var hour = 2;
        final var minute = 3;
        final var target = true;

        final var flipEntity = FlipEntity.with(flip, label, forced);
        final var pointEntity = PointEntity.with(flipEntity, day, hour, minute, target);

        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);
        when(entityService.changeFlipEntity(flipEntity, label, forced)).thenReturn(flipEntity);
        when(entityService.getOrCreatePointEntities(flipEntity)).thenReturn(List.of(pointEntity));

        final var pointDto = new PointDto(day, hour, minute, target);
        final var flipDto = new FlipDto(flip, label, forced, List.of(pointDto));

        final var payload = objectMapper.writeValueAsString(List.of(flipDto));

        mockMvc.perform(put(glanceUri()).contentType(APPLICATION_JSON).content(payload))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl(glanceUri().toString()));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).changeFlipEntity(flipEntity, label, forced);
        verify(entityService, times(1)).getOrCreatePointEntities(flipEntity);
        verify(entityService, times(1)).remove(pointEntity);
        verify(entityService, times(1)).createPointEntity(flipEntity, day, hour, minute, target);
        verifyNoMoreInteractions(entityService);
    }
}
