package su.toor.probe.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.http.MediaType.TEXT_HTML;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.net.URI;
import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.probe.FakeBean;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;
import su.toor.probe.resource.model.CoreModel;
import su.toor.probe.resource.model.FlipForm;
import su.toor.probe.resource.model.PointForm;
import su.toor.probe.service.EntityService;
import su.toor.probe.service.SerialService;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.StateDto;
import su.toor.probe.shared.Target;

@WebMvcTest(PageResource.class)
@Import(FakeBean.class)
class PageResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BuildProperties buildProperties;

    @MockitoBean
    private SerialService serialService;

    @MockitoBean
    private EntityService entityService;

    private static URI homeUri() {
        return UriComponentsBuilder.fromPath("/").build().toUri();
    }

    private static URI flipUri(final Flip flip) {
        return UriComponentsBuilder.fromPath("/flip/{flip}").build(Map.of("flip", flip));
    }

    private static URI flipEditUri(final Flip flip) {
        return UriComponentsBuilder.fromUri(flipUri(flip))
                .pathSegment("edit")
                .build()
                .toUri();
    }

    private static URI pointUri(final Flip flip, final boolean drop) {
        final var part = drop ? "drop" : "plus";
        return UriComponentsBuilder.fromUri(flipUri(flip))
                .pathSegment("point")
                .pathSegment(part)
                .build()
                .toUri();
    }

    @Test
    void getHomePage() throws Exception {
        final var state = new StateDto().setFlip1(true).setFlip2(false);

        when(serialService.getState()).thenReturn(state);

        mockMvc.perform(get(homeUri()).accept(TEXT_HTML))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("home"))
                .andExpect(
                        model().attribute(PageResource.FIELD_CORE, equalTo(CoreModel.with(buildProperties.getName()))))
                .andExpect(model().attribute(PageResource.FIELD_STATE, equalTo(state)))
                .andExpect(content().string(containsString(buildProperties.getName())))
                .andExpect(content()
                        .string(allOf(
                                containsString(Flip.FLIP_1.getLabel()),
                                containsString(Flip.FLIP_2.getLabel()),
                                containsString(Flip.FLIP_3.getLabel()),
                                containsString(Flip.FLIP_4.getLabel()),
                                not(containsString(Flip.UPDATE.getLabel())),
                                containsString(Target.ON.getLabel()),
                                containsString(Target.OFF.getLabel()))));

        verify(serialService, times(1)).getState();
        verifyNoMoreInteractions(serialService);
    }

    @Test
    void getFlipPage() throws Exception {
        final var flip = Flip.FLIP_2;
        final var label = "the-flip-label";
        final var forced = Target.TIMED;
        final var day = DayOfWeek.WEDNESDAY;
        final var hour = 22;
        final var minute = 11;
        final var target = true;

        final var flipEntity = FlipEntity.with(flip, label, forced.getState());
        final var pointEntity = PointEntity.with(flipEntity, day, hour, minute, target);

        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);
        when(entityService.getOrCreatePointEntities(flipEntity)).thenReturn(List.of(pointEntity));

        mockMvc.perform(get(flipUri(flip)).accept(TEXT_HTML))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("flip"))
                .andExpect(
                        model().attribute(PageResource.FIELD_CORE, equalTo(CoreModel.with(buildProperties.getName()))))
                .andExpect(model().attribute(PageResource.FIELD_FLIP, equalTo(flip)))
                .andExpect(model().attribute(PageResource.FIELD_FLIP_FORM, equalTo(FlipForm.from(flipEntity))))
                .andExpect(model().attribute(PageResource.FIELD_POINT_FORM, equalTo(PointForm.empty())))
                .andExpect(model().attribute(
                                PageResource.FIELD_POINT_FORMS, contains(equalTo(PointForm.from(pointEntity)))));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).getOrCreatePointEntities(flipEntity);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void editFlipPage() throws Exception {
        final var flip = Flip.FLIP_3;
        final var label = "some-label";
        final var forced = Target.ON;

        final var flipEntity = FlipEntity.with(flip, "old-label", Target.OFF.getState());
        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);

        mockMvc.perform(post(flipEditUri(flip))
                        .accept(TEXT_HTML)
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("label", label)
                        .param("forced", forced.name()))
                .andDo(print())
                .andExpect(status().isSeeOther())
                .andExpect(header().string("location", containsString("/flip")));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).changeFlipEntity(flipEntity, label, forced.getState());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void dropPointPageNotFound() throws Exception {
        final var flip = Flip.FLIP_1;
        final var day = DayOfWeek.SATURDAY;
        final var hour = 11;
        final var minute = 44;
        final var target = Target.OFF;

        final var flipEntity = FlipEntity.with(flip, "label");
        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);
        when(entityService.locatePointEntity(flipEntity, day, hour, minute, target.getState()))
                .thenReturn(Optional.empty());

        mockMvc.perform(post(pointUri(flip, true))
                        .accept(TEXT_HTML)
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("day", day.name())
                        .param("hour", String.valueOf(hour))
                        .param("minute", String.valueOf(minute))
                        .param("target", target.name()))
                .andDo(print())
                .andExpect(status().isSeeOther())
                .andExpect(header().string("location", containsString("/flip")));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).locatePointEntity(flipEntity, day, hour, minute, target.getState());
        verify(entityService, times(0)).remove(any());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void dropPointPage() throws Exception {
        final var flip = Flip.FLIP_2;
        final var day = DayOfWeek.SUNDAY;
        final var hour = 22;
        final var minute = 33;
        final var target = Target.ON;

        final var flipEntity = FlipEntity.with(flip, "label");
        final var pointEntity = PointEntity.with(flipEntity, day, hour, minute, target.getState());
        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);
        when(entityService.locatePointEntity(flipEntity, day, hour, minute, target.getState()))
                .thenReturn(Optional.of(pointEntity));

        mockMvc.perform(post(pointUri(flip, true))
                        .accept(TEXT_HTML)
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("day", day.name())
                        .param("hour", String.valueOf(hour))
                        .param("minute", String.valueOf(minute))
                        .param("target", target.name()))
                .andDo(print())
                .andExpect(status().isSeeOther())
                .andExpect(header().string("location", containsString("/flip")));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).locatePointEntity(flipEntity, day, hour, minute, target.getState());
        verify(entityService, times(1)).remove(pointEntity);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void plusPointPage() throws Exception {
        final var flip = Flip.FLIP_3;
        final var day = DayOfWeek.THURSDAY;
        final var hour = 13;
        final var minute = 12;
        final var target = Target.ON;

        final var flipEntity = FlipEntity.with(flip, "label");
        when(entityService.getOrCreateFlipEntity(flip)).thenReturn(flipEntity);

        mockMvc.perform(post(pointUri(flip, false))
                        .accept(TEXT_HTML)
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("day", day.name())
                        .param("hour", String.valueOf(hour))
                        .param("minute", String.valueOf(minute))
                        .param("target", target.name()))
                .andDo(print())
                .andExpect(status().isSeeOther())
                .andExpect(header().string("location", containsString("/flip")));

        verify(entityService, times(1)).getOrCreateFlipEntity(flip);
        verify(entityService, times(1)).createPointEntity(flipEntity, day, hour, minute, target.getState());
        verifyNoMoreInteractions(entityService);
    }
}
