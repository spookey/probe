package su.toor.probe;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;

class ApplicationTest {

    private static final String[] ARGS = new String[] {"some", "args"};

    @Test
    void success() {
        try (final var application = mockStatic(SpringApplication.class)) {

            assertThatNoException().isThrownBy(() -> Application.main(ARGS));

            application.verify(() -> SpringApplication.run(Application.class, ARGS));
        }
    }
}
