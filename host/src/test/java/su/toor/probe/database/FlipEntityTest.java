package su.toor.probe.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.probe.shared.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;
import su.toor.probe.shared.Flip;

class FlipEntityTest {

    @Test
    void createNull() {
        assertThatRequireException().isThrownBy(() -> FlipEntity.with(null, "label"));
    }

    @Test
    void create() {
        final var flip = Flip.FLIP_3;
        final var label = "label";
        final var forced = false;

        final var entity = FlipEntity.with(flip, label, forced);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getFlip()).isEqualTo(flip);
        assertThat(entity.getLabel()).isEqualTo(label);
        assertThat(entity.getForced()).isFalse();

        assertThat(entity.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(String.valueOf(flip))
                .contains(label)
                .contains(String.valueOf(forced));
    }

    @Test
    void label() {
        final var flip = Flip.FLIP_1;

        final var entity = FlipEntity.with(flip, null);

        assertThat(entity.getLabel()).isNotNull().isEqualTo(flip.getLabel());

        final var newLabel = "some label";

        assertThat(entity.setLabel(newLabel)).isSameAs(entity);
        assertThat(entity.getLabel()).isEqualTo(newLabel);

        assertThat(entity.setLabel(null)).isSameAs(entity);
        assertThat(entity.getLabel()).isEqualTo(flip.getLabel());
    }

    @Test
    void forced() {
        final var flip = Flip.FLIP_2;

        final var entity = FlipEntity.with(flip, "", true);
        assertThat(entity.getForced()).isTrue();

        assertThat(entity.setForced(false)).isSameAs(entity);
        assertThat(entity.getForced()).isFalse();

        assertThat(entity.setForced(null)).isSameAs(entity);
        assertThat(entity.getForced()).isNull();

        assertThat(entity.setForced(true)).isSameAs(entity);
        assertThat(entity.getForced()).isTrue();
    }
}
