package su.toor.probe.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.probe.shared.misc.RequireAssert.assertThatRequireException;

import java.time.DayOfWeek;
import org.junit.jupiter.api.Test;
import su.toor.probe.shared.Flip;

class PointEntityTest {

    private static final FlipEntity FLIP_ENTITY = FlipEntity.with(Flip.FLIP_1, "test");
    private static final DayOfWeek DAY = DayOfWeek.SATURDAY;
    private static final int HOUR = 23;
    private static final int MINUTE = 42;
    private static final boolean TARGET = true;

    @Test
    void createNull() {
        assertThatRequireException().isThrownBy(() -> PointEntity.with(null, DAY, HOUR, MINUTE, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, null, HOUR, MINUTE, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, null, MINUTE, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, HOUR, null, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, HOUR, MINUTE, null));
    }

    @Test
    void createBounds() {
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, -1, MINUTE, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, 24, MINUTE, TARGET));

        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, HOUR, -1, TARGET));
        assertThatRequireException().isThrownBy(() -> PointEntity.with(FLIP_ENTITY, DAY, HOUR, 60, TARGET));
    }

    @Test
    void create() {
        final var entity = PointEntity.with(FLIP_ENTITY, DAY, HOUR, MINUTE, TARGET);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getFlipEntity()).isEqualTo(FLIP_ENTITY);
        assertThat(entity.getDay()).isEqualTo(DAY);
        assertThat(entity.getHour()).isEqualTo(HOUR);
        assertThat(entity.getMinute()).isEqualTo(MINUTE);
        assertThat(entity.getTarget()).isEqualTo(TARGET);

        assertThat(entity.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(String.valueOf(FLIP_ENTITY))
                .contains(String.valueOf(DAY))
                .contains(String.valueOf(HOUR))
                .contains(String.valueOf(MINUTE))
                .contains(String.valueOf(TARGET));
    }
}
