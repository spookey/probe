package su.toor.probe.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;

import java.time.DayOfWeek;
import java.util.Comparator;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.DataBase;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.misc.Require;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class EntityHandlingTest extends DataBase {

    @Autowired
    private FlipRepository flipRepository;

    @Autowired
    private PointRepository pointRepository;

    @BeforeEach
    public void setup() {
        pointRepository.deleteAllInBatch();
        flipRepository.deleteAllInBatch();
    }

    @Test
    void uniqueFlip() {
        final var flip = Flip.FLIP_3;

        flipRepository.save(FlipEntity.with(flip, "label", true));
        final var newEntity = FlipEntity.with(flip, "new label", false);

        assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> flipRepository.save(newEntity));
    }

    @Test
    void pointMissingFlipEntity() {
        try (final var require = mockStatic(Require.class)) {
            require.when(() -> Require.notNull(anyString(), any(FlipEntity.class)))
                    .thenReturn(null);
            final var brokenEntity = PointEntity.with(null, DayOfWeek.MONDAY, 1, 1, true);

            assertThatExceptionOfType(DataIntegrityViolationException.class)
                    .isThrownBy(() -> pointRepository.save(brokenEntity));
        }
    }

    @Test
    void references() {
        final var flip = Flip.FLIP_2;
        final var dayOne = DayOfWeek.TUESDAY;
        final var dayTwo = DayOfWeek.SATURDAY;

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "label", null));
        final var pointEntityOne = pointRepository.save(PointEntity.with(flipEntity, dayOne, 16, 4, true));
        final var pointEntityTwo = pointRepository.save(PointEntity.with(flipEntity, dayTwo, 4, 16, false));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactlyInAnyOrder(pointEntityOne, pointEntityTwo);

        assertThat(pointEntityOne.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(pointEntityTwo.getFlipEntity()).isEqualTo(flipEntity);

        assertThat(pointRepository.allOfFlipEntity(flipEntity)).containsExactly(pointEntityOne, pointEntityTwo);

        assertThat(flipRepository.findByFlip(flip)).hasValue(flipEntity);
    }

    @Test
    void deleteCascade() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "label"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.WEDNESDAY, 12, 12, true));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        flipRepository.delete(flipEntity);

        assertThat(flipRepository.findAll()).isEmpty();
        assertThat(pointRepository.findAll()).isEmpty();
    }

    @Test
    void deleteCascadeReversed() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_1, "some label", null));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.TUESDAY, 11, 11, false));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        pointRepository.delete(pointEntity);

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();

        flipRepository.delete(flipEntity);

        assertThat(flipRepository.findAll()).isEmpty();
        assertThat(pointRepository.findAll()).isEmpty();
    }

    @Test
    void allOfFlipEntityOrdered() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_1, "label"));
        final var pointEntities = Set.of(
                pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.SUNDAY, 23, 59, false)),
                pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.FRIDAY, 13, 12, false)),
                pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.THURSDAY, 11, 11, true)),
                pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.THURSDAY, 11, 11, false)),
                pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.MONDAY, 0, 0, true)));

        assertThat(pointRepository.findAll()).containsExactlyInAnyOrderElementsOf(pointEntities);

        final Comparator<PointEntity> comparator =
                Comparator.comparingInt(pointEntity -> pointEntity.getDay().ordinal());
        assertThat(pointRepository.allOfFlipEntity(flipEntity))
                .containsExactlyElementsOf(pointEntities.stream()
                        .sorted(comparator
                                .thenComparingInt(PointEntity::getHour)
                                .thenComparingInt(PointEntity::getMinute)
                                .thenComparing(PointEntity::getTarget))
                        .toList());
    }
}
