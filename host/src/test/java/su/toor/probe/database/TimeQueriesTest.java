package su.toor.probe.database;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.DayOfWeek;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.DataBase;
import su.toor.probe.shared.Flip;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class TimeQueriesTest extends DataBase {

    @Autowired
    private FlipRepository flipRepository;

    @Autowired
    private PointRepository pointRepository;

    @BeforeEach
    public void setup() {
        pointRepository.deleteAllInBatch();
        flipRepository.deleteAllInBatch();
    }

    @Test
    void exactEmpty() {
        final var day = DayOfWeek.WEDNESDAY;
        final var hour = 13;
        final var minute = 12;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "two"));

        final var resultOne = pointRepository.locateExact(flipEntity, day, hour, minute, true);
        final var resultTwo = pointRepository.locateExact(flipEntity, day, hour, minute, false);
        assertThat(resultOne).isEmpty();
        assertThat(resultTwo).isEmpty();
    }

    @Test
    void exactSome() {
        final var day = DayOfWeek.FRIDAY;
        final var hour = 13;
        final var minute = 37;
        final var target = true;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "four"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, target));

        final var resultNone = pointRepository.locateExact(flipEntity, day, hour, minute, !target);
        final var resultFull = pointRepository.locateExact(flipEntity, day, hour, minute, target);
        assertThat(resultNone).isEmpty();
        assertThat(resultFull).hasValue(pointEntity);
    }

    @Test
    void beforeEmpty() {
        final var day = DayOfWeek.TUESDAY;
        final var hour = 13;
        final var minute = 12;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "three"));

        final var result = pointRepository.locateBefore(flipEntity, day, hour, minute);
        assertThat(result).isEmpty();
    }

    @Test
    void beforeExactMatch() {
        final var day = DayOfWeek.THURSDAY;
        final var hour = 23;
        final var minute = 24;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "three"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, true));

        final var result = pointRepository.locateBefore(flipEntity, day, hour, minute);
        assertThat(result).containsExactly(pointEntity);
    }

    @Test
    void beforeSameDay() {
        final var day = DayOfWeek.FRIDAY;
        final var hour = 2;
        final var minute = 3;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "three"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, false));

        IntStream.range(hour, 24).forEach(hrs -> IntStream.range(minute, 60).forEach(min -> {
            assertThat(hrs).isGreaterThanOrEqualTo(hour);
            assertThat(min).isGreaterThanOrEqualTo(minute);

            final var result = pointRepository.locateBefore(flipEntity, day, hrs, min);
            assertThat(result).containsExactly(pointEntity);
        }));
    }

    @Test
    void beforeLaterDays() {
        final var day = DayOfWeek.MONDAY;
        final var hour = 23;
        final var minute = 42;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "four"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, true));

        EnumSet.allOf(DayOfWeek.class).stream()
                .filter(dow -> dow.ordinal() > day.ordinal())
                .forEach(dow -> {
                    assertThat(dow).isGreaterThan(day);

                    final var resultTime = pointRepository.locateBefore(flipEntity, dow, hour, minute);
                    assertThat(resultTime).containsExactly(pointEntity);

                    final var resultOne = pointRepository.locateBefore(flipEntity, dow, hour, 0);
                    assertThat(resultOne).containsExactly(pointEntity);

                    final var resultTwo = pointRepository.locateBefore(flipEntity, dow, 0, minute);
                    assertThat(resultTwo).containsExactly(pointEntity);

                    final var resultMidnight = pointRepository.locateBefore(flipEntity, dow, 0, 0);
                    assertThat(resultMidnight).containsExactly(pointEntity);
                });
    }

    @Test
    void beforeUpperBound() {
        final var day = DayOfWeek.SUNDAY;
        final var hour = 23;
        final var minute = 59;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "two"));
        pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, true));

        EnumSet.allOf(DayOfWeek.class).stream()
                .filter(dow -> dow.ordinal() != day.ordinal())
                .forEach(dow -> IntStream.range(20, 24)
                        .forEach(hrs -> IntStream.range(50, 60).forEach(min -> {
                            assertThat(dow).isLessThan(day);
                            assertThat(hrs).isLessThanOrEqualTo(hour);
                            assertThat(min).isLessThanOrEqualTo(minute);

                            final var result = pointRepository.locateBefore(flipEntity, dow, hrs, min);
                            assertThat(result).isEmpty();
                        })));
    }

    @Test
    void beforeLowerBound() {
        final var day = DayOfWeek.MONDAY;
        final var hour = 0;
        final var minute = 0;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "three"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, false));

        EnumSet.allOf(DayOfWeek.class).forEach(dow -> IntStream.range(20, 24)
                .forEach(hrs -> IntStream.range(50, 60).forEach(min -> {
                    final var result = pointRepository.locateBefore(flipEntity, dow, hrs, min);
                    assertThat(result).containsExactly(pointEntity);
                })));
    }

    @Test
    void beforeMultiple() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "two"));
        final var pointEntities = EnumSet.allOf(DayOfWeek.class).stream()
                .flatMap(day -> IntStream.range(18, 20)
                        .boxed()
                        .map(hrs -> pointRepository.save(PointEntity.with(flipEntity, day, hrs, 0, true))))
                .collect(Collectors.toSet());

        EnumSet.allOf(DayOfWeek.class)
                .forEach(day -> IntStream.range(16, 22).boxed().forEach(hrs -> {
                    final Comparator<PointEntity> compare =
                            Comparator.comparingInt(pe -> pe.getDay().ordinal());
                    final var expect = pointEntities.stream()
                            .filter(pointEntity -> {
                                final var po = pointEntity.getDay().ordinal();
                                return po < day.ordinal() || (po == day.ordinal() && pointEntity.getHour() <= hrs);
                            })
                            .sorted(compare.thenComparingInt(PointEntity::getHour)
                                    .reversed())
                            .toList();

                    final var result = pointRepository.locateBefore(flipEntity, day, hrs, 0);
                    assertThat(result).containsExactlyElementsOf(expect);
                }));
    }
}
