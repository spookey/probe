package su.toor.probe.component.state;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;
import org.junit.jupiter.api.Test;
import su.toor.probe.shared.StateDto;

class StateResolverTest {

    @Test
    void fromBoolean() {
        assertThat(StateResolver.from((Boolean) null)).isZero();
        assertThat(StateResolver.from(false)).isZero();
        assertThat(StateResolver.from(true)).isOne();
    }

    @Test
    void fromNumber() {
        assertThat(StateResolver.from((Integer) null)).isZero();
        assertThat(StateResolver.from(0)).isZero();
        assertThat(StateResolver.from(1)).isOne();
        assertThat(StateResolver.from(23)).isEqualTo(23d);

        assertThat(StateResolver.from((Long) null)).isZero();
        assertThat(StateResolver.from(0L)).isZero();
        assertThat(StateResolver.from(1L)).isOne();
        assertThat(StateResolver.from(42L)).isEqualTo(42d);
    }

    @Test
    void nameAndType() {
        final var uniqueTagNames = new HashSet<String>();

        EnumSet.allOf(StateResolver.class).forEach(state -> {
            final var tagName = state.getTagName();
            final var tagType = state.getTagType();
            uniqueTagNames.add(tagName);

            assertThat(tagType).isNotNull().isNotEmpty().isNotBlank();

            assertThat(tagName).isNotNull().isNotEmpty().isNotBlank().startsWith(tagType);
        });

        assertThat(StateResolver.values()).hasSize(uniqueTagNames.size());
    }

    @Test
    void dtoResolve() {
        Map.of(
                        StateResolver.FLIP_1, Map.entry(0d, new StateDto().setFlip1(false)),
                        StateResolver.FLIP_2, Map.entry(1d, new StateDto().setFlip2(true)),
                        StateResolver.FLIP_3, Map.entry(0d, new StateDto().setFlip1(false)),
                        StateResolver.FLIP_4, Map.entry(1d, new StateDto().setFlip4(true)),
                        StateResolver.READ_1, Map.entry(0d, new StateDto().setRead1(false)),
                        StateResolver.READ_2, Map.entry(1d, new StateDto().setRead2(true)),
                        StateResolver.SENS_1, Map.entry(13d, new StateDto().setSens1(13)),
                        StateResolver.SENS_2, Map.entry(12d, new StateDto().setSens2(12)),
                        StateResolver.UPTIME, Map.entry(1337d, new StateDto().setUpNum(1337L)))
                .forEach((state, pair) -> {
                    final var resolver = state.getResolver();
                    final var expect = pair.getKey();
                    final var dto = pair.getValue();

                    assertThat(resolver).isNotNull();
                    final var result = resolver.applyAsDouble(dto);
                    assertThat(result).isEqualTo(expect);
                });
    }
}
