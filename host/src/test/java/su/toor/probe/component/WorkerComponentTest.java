package su.toor.probe.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.EnumMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.service.SerialService;
import su.toor.probe.service.TimerService;
import su.toor.probe.shared.Flip;

@ExtendWith(SpringExtension.class)
class WorkerComponentTest {

    @MockitoBean
    private TimerService timerService;

    @MockitoBean
    private SerialService serialService;

    @Test
    void constants() {
        assertThat(WorkerComponent.INTERVAL_TRANSMISSION_INITIAL).isPositive();
        assertThat(WorkerComponent.INTERVAL_TRANSMISSION_REGULAR)
                .isPositive()
                .isGreaterThanOrEqualTo(WorkerComponent.INTERVAL_TRANSMISSION_INITIAL);
    }

    @Test
    void transmission() {
        final var payload = Map.of(
                Flip.FLIP_1, true,
                Flip.FLIP_2, false,
                Flip.FLIP_3, true,
                Flip.FLIP_4, false);

        final var transmitted = new EnumMap<Flip, Boolean>(Flip.class);
        when(timerService.determineDesired()).thenReturn(payload);

        assertThat(transmitted).isEmpty();

        final var component = new WorkerComponent(timerService, serialService);
        component.sendTransmission();

        verify(timerService, times(1)).determineDesired();
        verify(serialService, times(1)).transmit(payload);
    }
}
