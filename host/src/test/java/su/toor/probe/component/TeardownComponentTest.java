package su.toor.probe.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.function.IntConsumer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class TeardownComponentTest {

    @MockitoBean
    private ConfigurableApplicationContext applicationContext;

    @Test
    void create() {
        final var component = new TeardownComponent();
        final var handler = component.createTeardown(applicationContext);

        assertThat(handler).isNotNull();
    }

    @Test
    void teardown() {
        final var exitCode = 42;

        final var consumer = mock(IntConsumer.class);
        final var handler = new TeardownComponent.TeardownHandler(applicationContext, consumer);

        handler.applicationQuit(exitCode);

        verify(consumer, times(1)).accept(exitCode);
        verifyNoMoreInteractions(consumer);
    }
}
