package su.toor.probe.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import java.util.ArrayList;
import java.util.EnumSet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.component.state.StateResolver;
import su.toor.probe.service.SerialService;

@ExtendWith(SpringExtension.class)
class MeterComponentTest {

    @MockitoBean
    private SerialService serialService;

    @Test
    void registration() {
        final var names = new ArrayList<String>();
        final var types = new ArrayList<String>();
        EnumSet.allOf(StateResolver.class).forEach(state -> {
            names.add(state.getTagName());
            types.add(state.getTagType());
        });

        final var registry = new SimpleMeterRegistry();
        final var component = new MeterComponent(registry, serialService);

        assertThat(registry.getMeters()).isEmpty();

        component.registerMetrics();

        verify(serialService, times(names.size())).getState();
        assertThat(registry.getMeters()).hasSize(types.size());

        registry.forEachMeter(meter -> {
            assertThat(meter).isInstanceOf(Gauge.class);

            final var id = meter.getId();
            assertThat(id.getName()).isEqualTo(MeterComponent.PROBE_STATE_NAME);
            assertThat(id.getDescription()).isEqualTo(MeterComponent.PROBE_STATE_DESCRIPTION);

            final var nameTag = id.getTag(MeterComponent.PROBE_STATE_TAG_NAME);
            assertThat(nameTag).isNotNull().isIn(names);
            final var typeTag = id.getTag(MeterComponent.PROBE_STATE_TAG_TYPE);
            assertThat(typeTag).isNotNull().isIn(types);
        });
    }
}
