package su.toor.probe.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.DataBase;
import su.toor.probe.appliaction.setting.BasicSetting;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.FlipRepository;
import su.toor.probe.database.PointEntity;
import su.toor.probe.database.PointRepository;
import su.toor.probe.shared.Flip;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class TimerServiceTest extends DataBase {

    private static final ZoneId TIMEZONE = ZoneId.of("Australia/Sydney");
    private static final BasicSetting BASIC_SETTING = new BasicSetting().setTimezone(TIMEZONE);

    @Autowired
    private FlipRepository flipRepository;

    @Autowired
    private PointRepository pointRepository;

    private EntityService entityService;

    @BeforeEach
    void setup() {
        pointRepository.deleteAllInBatch();
        flipRepository.deleteAllInBatch();

        entityService = new EntityService(flipRepository, pointRepository);
    }

    public static void verifyInitialPoint(final FlipEntity flipEntity, final PointEntity pointEntity) {
        assertThat(pointEntity.getId()).isNotNull();
        assertThat(pointEntity.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(pointEntity.getDay()).isEqualTo(EntityService.INITIAL_DAY);
        assertThat(pointEntity.getHour()).isEqualTo(EntityService.INITIAL_HOUR);
        assertThat(pointEntity.getMinute()).isEqualTo(EntityService.INITIAL_MINUTE);
        assertThat(pointEntity.getTarget()).isEqualTo(EntityService.INITIAL_TARGET);
    }

    @Test
    void getStamp() {
        final var service = new TimerService(BASIC_SETTING, entityService);

        final var start = ZonedDateTime.now(TIMEZONE);
        final var stamp = service.getStamp();

        assertThat(stamp).isNotNull().isBetween(start, start.plusSeconds(2));
    }

    @Test
    void searchPointEntityCreatesInitialPoint() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "two"));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.searchPointEntity(flipEntity, start);

        verifyInitialPoint(flipEntity, result);

        assertThat(pointRepository.findAll()).containsExactly(result);
    }

    @Test
    void searchPointEntityFindsInPast() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var then = start.minusHours(3);

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_1, "one"));
        final var pointEntity = pointRepository.save(
                PointEntity.with(flipEntity, then.getDayOfWeek(), then.getHour(), then.getMinute(), true));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.searchPointEntity(flipEntity, start);

        assertThat(result).isEqualTo(pointEntity);
        assertThat(pointRepository.findAll()).containsExactly(result);
    }

    @Test
    void searchPointEntityOverCompleteWeek() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var then = start.plusMinutes(1);

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "three"));
        final var pointEntity = pointRepository.save(
                PointEntity.with(flipEntity, then.getDayOfWeek(), then.getHour(), then.getMinute(), true));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.searchPointEntity(flipEntity, start);

        assertThat(result).isEqualTo(pointEntity);
        assertThat(pointRepository.findAll()).containsExactly(result);
    }

    @Test
    void determineDesiredForced() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var flip = Flip.FLIP_4;
        final var forcedTarget = false;

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "label", forcedTarget));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.determineDesired(flipEntity, start);

        assertThat(result).hasValue(Map.entry(flip, forcedTarget));

        final var pointEntities = pointRepository.findAll();
        assertThat(pointEntities).hasSize(1);

        final var pointEntity = pointEntities.stream().iterator().next();
        verifyInitialPoint(flipEntity, pointEntity);
    }

    @Test
    void determineDesiredInternal() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var flip = Flip.UPDATE;
        assertThat(flip.isInternal()).isTrue();

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "internal"));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.determineDesired(flipEntity, start);

        assertThat(result).isEmpty();

        final var pointEntities = pointRepository.findAll();
        assertThat(pointEntities).hasSize(1);

        final var pointEntity = pointEntities.stream().iterator().next();
        verifyInitialPoint(flipEntity, pointEntity);
    }

    @Test
    void determineDesiredSingle() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var then = start.plusDays(2);
        final var flip = Flip.FLIP_2;
        final var target = true;

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "label"));
        pointRepository.save(
                PointEntity.with(flipEntity, then.getDayOfWeek(), then.getHour(), then.getMinute(), target));

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.determineDesired(flipEntity, start);

        assertThat(result).hasValue(Map.entry(flip, target));
    }

    @Test
    void determineDesiredCreates() {
        assertThat(flipRepository.findAll()).isEmpty();
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.determineDesired();

        assertThat(result).isNotNull().isNotEmpty();

        EnumSet.allOf(Flip.class).forEach(flip -> {
            if (!flip.isInternal()) {
                assertThat(result).containsKey(flip);
            }

            final var optionalFlipEntity = flipRepository.findByFlip(flip);
            assertThat(optionalFlipEntity).isPresent();

            final var flipEntity = optionalFlipEntity.get();
            assertThat(flipEntity.getId()).isNotNull();
            assertThat(flipEntity.getFlip()).isEqualTo(flip);
            assertThat(flipEntity.getLabel()).isEqualTo(flip.getLabel());
            assertThat(flipEntity.getForced()).isNull();

            final var pointEntities = pointRepository.allOfFlipEntity(flipEntity);
            assertThat(pointEntities).hasSize(1);

            final var pointEntity = pointEntities.stream().iterator().next();
            verifyInitialPoint(flipEntity, pointEntity);
        });
    }

    @Test
    void determineDesiredOneForcedTwoTimed() {
        final var start = ZonedDateTime.now(TIMEZONE);
        final var then = start.minusMinutes(13);

        final var forced = true;
        final var targetOne = true;
        final var targetTwo = false;

        flipRepository.save(FlipEntity.with(Flip.FLIP_1, "forced", forced));

        final var flipEntityOne = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "timed"));
        pointRepository.save(
                PointEntity.with(flipEntityOne, then.getDayOfWeek(), then.getHour(), then.getMinute(), targetOne));

        final var flipEntityTwo = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "timed"));
        pointRepository.save(
                PointEntity.with(flipEntityTwo, then.getDayOfWeek(), then.getHour(), then.getMinute(), targetTwo));

        final var expect = Map.of(
                Flip.FLIP_1, forced,
                Flip.FLIP_2, EntityService.INITIAL_TARGET,
                Flip.FLIP_3, targetOne,
                Flip.FLIP_4, targetTwo);

        final var service = new TimerService(BASIC_SETTING, entityService);
        final var result = service.determineDesired();

        assertThat(result).isNotNull().isNotEmpty().containsExactlyInAnyOrderEntriesOf(expect);
    }
}
