package su.toor.probe.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.time.DayOfWeek;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Optional;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.DataBase;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.FlipRepository;
import su.toor.probe.database.PointEntity;
import su.toor.probe.database.PointRepository;
import su.toor.probe.shared.Flip;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class EntityServiceTest extends DataBase {

    @Autowired
    private FlipRepository flipRepository;

    @Autowired
    private PointRepository pointRepository;

    @BeforeEach
    public void setup() {
        pointRepository.deleteAllInBatch();
        flipRepository.deleteAllInBatch();
    }

    @Test
    void updateFlipEntity() {
        final var flip = Flip.FLIP_4;
        final var label = "some-label";
        final var forced = false;

        final var flipEntity = FlipEntity.with(flip, label, forced);

        assertThat(flipRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.update(flipEntity);

        assertThat(result.getId()).isNotNull();
        assertThat(result.getFlip()).isEqualTo(flip);
        assertThat(result.getLabel()).isEqualTo(label);
        assertThat(result.getForced()).isEqualTo(forced);

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
    }

    @Test
    void updateFlipEntityFails() {
        final var flip = Flip.FLIP_3;

        final var existingFlipEntity = flipRepository.save(FlipEntity.with(flip, "existing"));
        final var duplicateFlipEntity = FlipEntity.with(flip, "duplicate");

        assertThat(flipRepository.findAll()).containsExactly(existingFlipEntity);

        final var service = new EntityService(flipRepository, pointRepository);
        assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> service.update(duplicateFlipEntity));
    }

    @Test
    void updatePointEntity() {
        final var day = DayOfWeek.WEDNESDAY;
        final var hour = 12;
        final var minute = 16;
        final var target = true;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_1, "some-label"));
        final var pointEntity = PointEntity.with(flipEntity, day, hour, minute, target);

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.update(pointEntity);

        assertThat(result.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(result.getDay()).isEqualTo(day);
        assertThat(result.getHour()).isEqualTo(hour);
        assertThat(result.getMinute()).isEqualTo(minute);
        assertThat(result.getTarget()).isEqualTo(target);

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);
    }

    @Test
    void updatePointEntityFails() {
        final var flipEntity = FlipEntity.with(Flip.FLIP_3, "label");
        final var pointEntity = PointEntity.with(flipEntity, DayOfWeek.TUESDAY, 8, 24, true);

        assertThat(flipRepository.findAll()).isEmpty();
        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        assertThatExceptionOfType(InvalidDataAccessApiUsageException.class)
                .isThrownBy(() -> service.update(pointEntity));
    }

    @Test
    void removePointEntity() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_2, "label"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.THURSDAY, 7, 23, false));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        final var service = new EntityService(flipRepository, pointRepository);
        service.remove(pointEntity);

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).isEmpty();
    }

    @Test
    void getOrCreateFlipEntityExisting() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "some", true));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreateFlipEntity(flipEntity.getFlip());

        assertThat(result).isEqualTo(flipEntity).isSameAs(flipEntity);
        assertThat(flipRepository.findAll()).containsExactly(result);
    }

    @Test
    void getOrCreateFlipEntityCreate() {
        final var flip = Flip.FLIP_2;

        assertThat(flipRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreateFlipEntity(flip);

        assertThat(result.getId()).isNotNull();
        assertThat(result.getFlip()).isEqualTo(flip);
        assertThat(result.getLabel()).isEqualTo(flip.getLabel());
        assertThat(result.getForced()).isNull();

        assertThat(flipRepository.findAll()).containsExactly(result);
    }

    @Test
    void getOrCreateFlipEntitiesCreateAll() {
        assertThat(flipRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreateFlipEntities();

        final var expected = EnumSet.allOf(Flip.class).stream()
                .sorted(Comparator.comparingInt(Flip::ordinal))
                .map(flipRepository::findByFlip)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        assertThat(result).containsExactlyElementsOf(expected);

        EnumSet.allOf(Flip.class).forEach(flip -> {
            final var optionalFlipEntity = flipRepository.findByFlip(flip);
            assertThat(optionalFlipEntity).isPresent();

            final var flipEntity = optionalFlipEntity.get();
            assertThat(flipEntity.getId()).isNotNull();
            assertThat(flipEntity.getFlip()).isEqualTo(flip);
            assertThat(flipEntity.getLabel()).isEqualTo(flip.getLabel());
            assertThat(flipEntity.getForced()).isNull();
        });
    }

    @Test
    void getOrCreateFlipEntitiesCreateRemaining() {
        final var flipEntityThree = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "some", true));
        final var flipEntityUpdate = flipRepository.save(FlipEntity.with(Flip.UPDATE, "internal", null));

        assertThat(flipRepository.findAll()).containsExactlyInAnyOrder(flipEntityThree, flipEntityUpdate);

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreateFlipEntities();

        final var expected = EnumSet.allOf(Flip.class).stream()
                .sorted(Comparator.comparingInt(Flip::ordinal))
                .map(flipRepository::findByFlip)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        assertThat(result).containsExactlyElementsOf(expected);

        EnumSet.allOf(Flip.class).forEach(flip -> {
            final var optionalFlipEntity = flipRepository.findByFlip(flip);
            assertThat(optionalFlipEntity).isPresent();

            final var flipEntity = optionalFlipEntity.get();
            switch (flip) {
                case FLIP_3 -> assertThat(flipEntity).isEqualTo(flipEntityThree);
                case UPDATE -> assertThat(flipEntity).isEqualTo(flipEntityUpdate);
                default -> {
                    assertThat(flipEntity.getId()).isNotNull();
                    assertThat(flipEntity.getFlip()).isEqualTo(flip);
                    assertThat(flipEntity.getLabel()).isEqualTo(flip.getLabel());
                    assertThat(flipEntity.getForced()).isNull();
                }
            }
        });
    }

    @Test
    void createPointEntity() {
        final var day = DayOfWeek.THURSDAY;
        final var hour = 13;
        final var minute = 12;
        final var target = false;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_3, "some"));

        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.createPointEntity(flipEntity, day, hour, minute, target);

        assertThat(result.getId()).isNotNull();
        assertThat(result.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(result.getDay()).isEqualTo(day);
        assertThat(result.getHour()).isEqualTo(hour);
        assertThat(result.getMinute()).isEqualTo(minute);
        assertThat(result.getTarget()).isEqualTo(target);

        assertThat(pointRepository.findAll()).containsExactly(result);
    }

    @Test
    void createInitialPointEntity() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "some"));

        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.createInitialPointEntity(flipEntity);

        assertThat(result).isNotNull();

        assertThat(result.getId()).isNotNull();
        assertThat(result.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(result.getDay()).isEqualTo(EntityService.INITIAL_DAY);
        assertThat(result.getHour()).isEqualTo(EntityService.INITIAL_HOUR);
        assertThat(result.getMinute()).isEqualTo(EntityService.INITIAL_MINUTE);
        assertThat(result.getTarget()).isEqualTo(EntityService.INITIAL_TARGET);

        assertThat(pointRepository.findAll()).containsExactly(result);
    }

    @Test
    void getOrCreatePointEntitiesExisting() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "label", false));

        final var pointEntities = IntStream.range(0, 24)
                .mapToObj(hour -> pointRepository.save(PointEntity.with(flipEntity, DayOfWeek.TUESDAY, hour, 0, true)))
                .toList();

        assertThat(pointRepository.findAll()).containsExactlyInAnyOrderElementsOf(pointEntities);

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreatePointEntities(flipEntity);

        assertThat(result).containsExactlyElementsOf(pointEntities);

        assertThat(pointRepository.findAll()).containsExactlyInAnyOrderElementsOf(pointEntities);
    }

    @Test
    void getOrCreatePointEntitiesCreate() {
        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_1, "label"));

        assertThat(pointRepository.findAll()).isEmpty();

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.getOrCreatePointEntities(flipEntity);

        assertThat(result).isNotEmpty().hasSize(1);

        final var pointEntity = result.getFirst();
        assertThat(pointEntity).isNotNull();

        assertThat(pointEntity.getId()).isNotNull();
        assertThat(pointEntity.getFlipEntity()).isEqualTo(flipEntity);
        assertThat(pointEntity.getDay()).isEqualTo(EntityService.INITIAL_DAY);
        assertThat(pointEntity.getHour()).isEqualTo(EntityService.INITIAL_HOUR);
        assertThat(pointEntity.getMinute()).isEqualTo(EntityService.INITIAL_MINUTE);
        assertThat(pointEntity.getTarget()).isEqualTo(EntityService.INITIAL_TARGET);
    }

    @Test
    void changeFlipEntity() {
        final var flip = Flip.FLIP_3;
        final var label = "some-label";
        final var forced = false;

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "other-label", !forced));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);

        final var service = new EntityService(flipRepository, pointRepository);
        final var result = service.changeFlipEntity(flipEntity, label, forced);

        assertThat(result.getId()).isSameAs(flipEntity.getId());
        assertThat(result.getFlip()).isEqualTo(flip);
        assertThat(result.getLabel()).isEqualTo(label);
        assertThat(result.getForced()).isEqualTo(forced);

        assertThat(flipRepository.findAll()).containsExactly(result);
    }

    @Test
    void locatePointEntity() {
        final var day = DayOfWeek.TUESDAY;
        final var hour = 13;
        final var minute = 12;
        final var target = false;

        final var flipEntity = flipRepository.save(FlipEntity.with(Flip.FLIP_4, "label"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, target));

        final var service = new EntityService(flipRepository, pointRepository);

        final var resultNone = service.locatePointEntity(flipEntity, day, hour, minute, !target);
        final var resultFull = service.locatePointEntity(flipEntity, day, hour, minute, target);

        assertThat(resultNone).isEmpty();
        assertThat(resultFull).hasValue(pointEntity);
    }

    @Test
    void latestPointEntity() {
        final var flip = Flip.FLIP_3;
        final var day = DayOfWeek.THURSDAY;
        final var hour = 12;
        final var minute = 30;

        final var flipEntity = flipRepository.save(FlipEntity.with(flip, "label"));
        final var pointEntity = pointRepository.save(PointEntity.with(flipEntity, day, hour, minute, true));

        assertThat(flipRepository.findAll()).containsExactly(flipEntity);
        assertThat(pointRepository.findAll()).containsExactly(pointEntity);

        final var service = new EntityService(flipRepository, pointRepository);

        // query past
        assertThat(service.latestPointEntity(flipEntity, day.minus(1), 0, 0)).isEmpty();
        assertThat(service.latestPointEntity(flipEntity, day, 0, 0)).isEmpty();
        assertThat(service.latestPointEntity(flipEntity, day, hour - 1, 0)).isEmpty();
        assertThat(service.latestPointEntity(flipEntity, day, hour, 0)).isEmpty();
        assertThat(service.latestPointEntity(flipEntity, day, hour, minute - 1)).isEmpty();

        // exact match
        assertThat(service.latestPointEntity(flipEntity, day, hour, minute)).hasValue(pointEntity);

        // query future
        assertThat(service.latestPointEntity(flipEntity, day, hour, minute + 1)).hasValue(pointEntity);
        assertThat(service.latestPointEntity(flipEntity, day, hour, 59)).hasValue(pointEntity);
        assertThat(service.latestPointEntity(flipEntity, day, hour + 1, 0)).hasValue(pointEntity);
        assertThat(service.latestPointEntity(flipEntity, day, 23, 0)).hasValue(pointEntity);
        assertThat(service.latestPointEntity(flipEntity, day.plus(1), 0, 0)).hasValue(pointEntity);
    }
}
