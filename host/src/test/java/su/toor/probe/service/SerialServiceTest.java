package su.toor.probe.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.probe.communication.Communicator;
import su.toor.probe.communication.core.ex.UninitializedException;
import su.toor.probe.communication.core.ex.WriterException;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.StateDto;

@ExtendWith(SpringExtension.class)
class SerialServiceTest {

    @MockitoBean
    private Communicator communicator;

    @Test
    void state() {
        final var state = new StateDto()
                .setFlip1(true)
                .setFlip2(false)
                .setFlip3(true)
                .setFlip4(false)
                .setRead1(true)
                .setRead2(false)
                .setSens1(13)
                .setSens2(12)
                .setUpNum(0L)
                .setUpStr("zero");

        when(communicator.getState()).thenReturn(state);

        final var service = new SerialService(communicator);

        assertThat(service.getState()).isEqualTo(state);

        verify(communicator, times(1)).getState();
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void transmitUninitialized() throws UninitializedException, WriterException {
        doAnswer(invocation -> {
                    throw new UninitializedException("/some/port");
                })
                .when(communicator)
                .message(anyString());

        final var service = new SerialService(communicator);

        assertThatNoException().isThrownBy(() -> service.transmit("message"));

        verify(communicator, times(1)).message(anyString());
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void transmitWriteError() throws UninitializedException, WriterException {
        doAnswer(invocation -> {
                    throw new WriterException(new IOException("error!"));
                })
                .when(communicator)
                .message(anyString());

        final var service = new SerialService(communicator);

        assertThatNoException().isThrownBy(() -> service.transmit("message"));

        verify(communicator, times(1)).message(anyString());
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void transmit() throws UninitializedException, WriterException {
        final var payload = "some command";
        doAnswer(invocation -> {
                    final var message = invocation.getArgument(0, String.class);
                    assertThat(message).isEqualTo(payload);

                    return null;
                })
                .when(communicator)
                .message(anyString());

        final var service = new SerialService(communicator);

        service.transmit(payload);

        verify(communicator, times(1)).message(anyString());
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void transmitBatchEmpty() throws UninitializedException, WriterException {
        final var payload = Flip.UPDATE.getMessage(true);
        doAnswer(invocation -> {
                    final var message = invocation.getArgument(0, String.class);
                    assertThat(message).isEqualTo(payload);

                    return null;
                })
                .when(communicator)
                .message(anyString());

        final var service = new SerialService(communicator);

        service.transmit(Map.of());

        verify(communicator, times(1)).message(anyString());
        verifyNoMoreInteractions(communicator);
    }

    @Test
    void transmitBatch() throws UninitializedException, WriterException {
        final var desired = Map.of(
                Flip.FLIP_1, false,
                Flip.FLIP_2, true,
                Flip.FLIP_3, false,
                Flip.FLIP_4, true);
        final var expected = desired.entrySet().stream()
                .map(entry -> entry.getKey().getMessage(entry.getValue()))
                .collect(Collectors.toSet());

        doAnswer(invocation -> {
                    final var message = invocation.getArgument(0, String.class);
                    expected.forEach(expect -> assertThat(message).contains(expect));

                    return null;
                })
                .when(communicator)
                .message(anyString());

        final var service = new SerialService(communicator);

        service.transmit(desired);

        verify(communicator, times(1)).message(anyString());
        verifyNoMoreInteractions(communicator);
    }
}
