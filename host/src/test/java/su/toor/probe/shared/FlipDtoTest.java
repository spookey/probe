package su.toor.probe.shared;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.probe.shared.misc.RequireAssert.assertThatRequireException;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import su.toor.probe.database.FlipEntity;

class FlipDtoTest {

    @Test
    void createInvalid() {
        final var flip = Flip.FLIP_2;
        final var label = "some";
        final var forced = true;
        final List<PointDto> points = List.of();

        assertThatRequireException().isThrownBy(() -> new FlipDto(null, label, forced, points));
        assertThatRequireException().isThrownBy(() -> new FlipDto(flip, label, forced, null));

        assertThatNoException().isThrownBy(() -> new FlipDto(flip, null, forced, points));
        assertThatNoException().isThrownBy(() -> new FlipDto(flip, label, null, points));
    }

    @Test
    void create() {
        final var pointDto = new PointDto(DayOfWeek.MONDAY, 23, 42, true);

        final var flip = Flip.FLIP_4;
        final var label = "thing";
        final var forced = false;
        final var points = List.of(pointDto);

        final var dto = new FlipDto(flip, label, forced, points);

        assertThat(dto.flip()).isEqualTo(flip);
        assertThat(dto.label()).isEqualTo(label);
        assertThat(dto.forced()).isEqualTo(forced);
        assertThat(dto.points()).containsExactly(pointDto);

        assertThat(dto.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(flip.name())
                .contains(label)
                .contains(String.valueOf(forced))
                .contains(String.valueOf(points));
    }

    @Test
    void from() {
        final var flipEntity = FlipEntity.with(Flip.FLIP_3, "flip", true);

        final var dto = FlipDto.from(flipEntity, Set.of());

        assertThat(dto.flip()).isEqualTo(flipEntity.getFlip());
        assertThat(dto.label()).isEqualTo(flipEntity.getLabel());
        assertThat(dto.forced()).isEqualTo(flipEntity.getForced());
        assertThat(dto.points()).isEmpty();
    }
}
