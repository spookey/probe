package su.toor.probe.shared;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class TargetTest {

    @Test
    void resolve() {
        assertThat(Target.resolve(true)).isEqualTo(Target.ON);
        assertThat(Target.resolve(false)).isEqualTo(Target.OFF);
        assertThat(Target.resolve(null)).isEqualTo(Target.TIMED);
        assertThat(Target.resolve(Boolean.TRUE)).isEqualTo(Target.ON);
        assertThat(Target.resolve(Boolean.FALSE)).isEqualTo(Target.OFF);
    }

    @ParameterizedTest
    @EnumSource(Target.class)
    void labels(final Target target) {
        assertThat(target.getLabel()).isNotNull().isNotEmpty().isNotBlank();
    }

    @Test
    void states() {
        assertThat(Target.TIMED.getState()).isNull();
        assertThat(Target.OFF.getState()).isFalse();
        assertThat(Target.ON.getState()).isTrue();
    }

    @ParameterizedTest
    @EnumSource(Target.class)
    void enforce(final Target target) {
        final var expect = target == Target.TIMED;

        assertThat(target.isEnforce()).isEqualTo(expect);
    }
}
