package su.toor.probe.shared;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.probe.shared.misc.RequireAssert.assertThatRequireException;

import java.time.DayOfWeek;
import org.junit.jupiter.api.Test;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;

class PointDtoTest {

    @Test
    void createInvalid() {
        final var day = DayOfWeek.TUESDAY;
        final var hour = 23;
        final var minute = 42;
        final var target = true;

        assertThatRequireException().isThrownBy(() -> new PointDto(null, hour, minute, target));
        assertThatRequireException().isThrownBy(() -> new PointDto(day, null, minute, target));
        assertThatRequireException().isThrownBy(() -> new PointDto(day, hour, null, target));
        assertThatRequireException().isThrownBy(() -> new PointDto(day, hour, minute, null));
        assertThatRequireException().isThrownBy(() -> new PointDto(day, 24, minute, target));
        assertThatRequireException().isThrownBy(() -> new PointDto(day, hour, 60, target));
    }

    @Test
    void create() {
        final var day = DayOfWeek.MONDAY;
        final var hour = 23;
        final var minute = 42;
        final var target = true;

        final var dto = new PointDto(day, hour, minute, target);

        assertThat(dto.day()).isEqualTo(day);
        assertThat(dto.hour()).isEqualTo(hour);
        assertThat(dto.minute()).isEqualTo(minute);
        assertThat(dto.target()).isEqualTo(target);

        assertThat(dto.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(day.name())
                .contains(String.valueOf(hour))
                .contains(String.valueOf(minute))
                .contains(String.valueOf(target));
    }

    @Test
    void from() {
        final var flipEntity = FlipEntity.with(Flip.FLIP_4, "flip");
        final var pointEntity = PointEntity.with(flipEntity, DayOfWeek.SATURDAY, 23, 42, false);

        final var dto = PointDto.from(pointEntity);

        assertThat(dto.day()).isEqualTo(pointEntity.getDay());
        assertThat(dto.hour()).isEqualTo(pointEntity.getHour());
        assertThat(dto.minute()).isEqualTo(pointEntity.getMinute());
        assertThat(dto.target()).isEqualTo(pointEntity.getTarget());
    }
}
