package su.toor.probe.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class FlipTest {

    @ParameterizedTest
    @EnumSource(Flip.class)
    void messages(final Flip flip) {
        Map.of(
                        true, Map.entry(":", "f"),
                        false, Map.entry(".", "n"))
                .forEach((target, parts) -> {
                    final var message = flip.getMessage(target);
                    final var expect = flip == Flip.UPDATE ? parts.getKey() : parts.getValue();

                    assertThat(message)
                            .isNotNull()
                            .isNotEmpty()
                            .isNotBlank()
                            .hasSize(2)
                            .endsWith(expect);
                });
    }

    @ParameterizedTest
    @EnumSource(Flip.class)
    void internal(final Flip flip) {
        final var expect = flip == Flip.UPDATE;

        assertThat(flip.isInternal()).isEqualTo(expect);
    }

    @ParameterizedTest
    @EnumSource(Flip.class)
    void label(final Flip flip) {
        assertThat(flip.getLabel()).isNotNull().isNotEmpty().isNotBlank();
    }
}
