package su.toor.probe.shared.misc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static su.toor.probe.shared.misc.RequireAssert.assertThatRequireException;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class RequireTest {

    @Nested
    class RequireExceptionTest {

        @ParameterizedTest
        @ValueSource(strings = {"error", "message"})
        @NullAndEmptySource
        void create(final String message) {
            assertThat(new Require.RequireException(message)).hasMessage(message);
        }
    }

    @Test
    void condition() {
        final var message = "some error message";
        final String value = "content";

        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(null, message, 23))
                .withMessage("check is null");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "", 42))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "\t", 42))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, null, 23))
                .withMessage("message is null, empty or blank");

        assertThatRequireException()
                .isThrownBy(() -> Require.condition(String::isEmpty, message, value))
                .withMessage(message);

        assertThat(Require.condition(Predicate.not(String::isEmpty), message, value))
                .isEqualTo(value);
    }

    @Test
    void notNull() {
        final String fieldName = "theText";
        final var text = "text";

        assertThatRequireException()
                .isThrownBy(() -> Require.notNull(fieldName, null))
                .withMessageContaining(fieldName);

        assertThat(Require.notNull(fieldName, text)).isEqualTo(text);
    }

    @Test
    void betweenIncluding() {
        final var fieldName = "theNumber";

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, null, 23, 42))
                .withMessage(fieldName + " is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, 0, null, 42))
                .withMessage("lowerIncluding is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, 0, 23, null))
                .withMessage("upperIncluding is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, 0, 42, 23))
                .withMessage("Idiot!");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, 22, 23, 42))
                .withMessage(fieldName + " is too low");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenIncluding(fieldName, 43, 23, 42))
                .withMessage(fieldName + " is too high");

        IntStream.rangeClosed(23, 42).forEach(num -> assertThat(Require.betweenIncluding(fieldName, num, 23, 42))
                .isEqualTo(num));
    }
}
