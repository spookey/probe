package su.toor.probe.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class StateDtoTest {

    private static void verifyEmpty(final StateDto state) {
        assertThat(state.getFlip1()).isFalse();
        assertThat(state.getFlip2()).isFalse();
        assertThat(state.getFlip3()).isFalse();
        assertThat(state.getFlip4()).isFalse();
        assertThat(state.getRead1()).isFalse();
        assertThat(state.getRead2()).isFalse();
        assertThat(state.getSens1()).isZero();
        assertThat(state.getSens2()).isZero();
        assertThat(state.getUpNum()).isZero();
        assertThat(state.getUpStr()).isEmpty();
    }

    @Test
    void initial() {
        verifyEmpty(new StateDto());
    }

    @Test
    void set() {
        final var base = new StateDto();
        verifyEmpty(base);

        final var sens1 = 13;
        final var sens2 = 12;
        final var upNum = 42L;
        final var upStr = "00:00:%d".formatted(upNum);

        final var temp = new StateDto()
                .setFlip1(true)
                .setFlip2(false)
                .setFlip3(true)
                .setFlip4(false)
                .setRead1(true)
                .setRead2(false)
                .setSens1(sens1)
                .setSens2(sens2)
                .setUpNum(upNum)
                .setUpStr(upStr);

        final var repl = base.set(temp);

        assertThat(base).isSameAs(repl).isNotSameAs(temp).isEqualTo(repl).isEqualTo(temp);

        Stream.of(base, repl).forEach(state -> {
            assertThat(state.getFlip1()).isTrue();
            assertThat(state.getFlip2()).isFalse();
            assertThat(state.getFlip3()).isTrue();
            assertThat(state.getFlip4()).isFalse();
            assertThat(state.getRead1()).isTrue();
            assertThat(state.getRead2()).isFalse();
            assertThat(state.getSens1()).isEqualTo(sens1);
            assertThat(state.getSens2()).isEqualTo(sens2);
            assertThat(state.getUpNum()).isEqualTo(upNum);
            assertThat(state.getUpStr()).isEqualTo(upStr);

            assertThat(state.toString())
                    .isNotNull()
                    .isNotEmpty()
                    .isNotBlank()
                    .contains(String.valueOf(true))
                    .contains(String.valueOf(false))
                    .contains(String.valueOf(sens1))
                    .contains(String.valueOf(sens2))
                    .contains(String.valueOf(upNum))
                    .contains(String.valueOf(upStr));
        });
    }

    @Test
    void upStr() {
        final var up = "00:00:23";
        final var dto = new StateDto();
        assertThat(dto.getUpStr()).isNotNull().isEmpty();

        assertThat(dto.setUpStr(up)).isSameAs(dto);
        assertThat(dto.getUpStr()).isEqualTo(up);

        assertThat(dto.setUpStr("")).isSameAs(dto);
        assertThat(dto.getUpStr()).isNotNull().isEmpty();

        assertThat(dto.setUpStr(up)).isSameAs(dto);
        assertThat(dto.getUpStr()).isEqualTo(up);

        assertThat(dto.setUpStr(null)).isSameAs(dto);
        assertThat(dto.getUpStr()).isNotNull().isEmpty();
    }

    @Test
    void equalsAndHash() {
        final var empty = new StateDto();
        final var upStr = new StateDto().setUpStr("oh no");

        final var reference = new StateDto();
        assertThat(reference)
                .hasSameHashCodeAs(reference)
                .isEqualTo(empty)
                .hasSameHashCodeAs(empty)
                .isNotEqualTo(new StateDto().setFlip1(true))
                .isNotEqualTo(new StateDto().setFlip2(true))
                .isNotEqualTo(new StateDto().setFlip3(true))
                .isNotEqualTo(new StateDto().setFlip4(true))
                .isNotEqualTo(new StateDto().setRead1(true))
                .isNotEqualTo(new StateDto().setRead2(true))
                .isNotEqualTo(new StateDto().setSens1(23))
                .isNotEqualTo(new StateDto().setSens2(42))
                .isNotEqualTo(new StateDto().setUpNum(1337L))
                .isNotEqualTo(upStr)
                .doesNotHaveSameHashCodeAs(upStr)
                .isNotEqualTo(new Object());
    }
}
