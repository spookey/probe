package su.toor.probe.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.toor.probe.communication.Communicator;
import su.toor.probe.communication.core.ex.UninitializedException;
import su.toor.probe.communication.core.ex.WriterException;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.StateDto;

@Service
public class SerialService {

    private static final Logger LOG = LoggerFactory.getLogger(SerialService.class);

    static final String FIELD_MESSAGE = "message";

    private final Communicator communicator;

    @Autowired
    public SerialService(final Communicator communicator) {
        this.communicator = communicator;
    }

    public StateDto getState() {
        return communicator.getState();
    }

    void transmit(final String message) {

        try {
            communicator.message(message);
        } catch (final UninitializedException ex) {
            LOG.warn(
                    "message on uninitialized port [{}] [{}]",
                    keyValue(FIELD_MESSAGE, message),
                    keyValue("exception", ex.getMessage()));
        } catch (final WriterException ex) {
            LOG.error("message failed [{}]", keyValue(FIELD_MESSAGE, message), ex);
        }
    }

    public void transmit(final Map<Flip, Boolean> desired) {
        if (desired.isEmpty()) {
            transmit(Flip.UPDATE.getMessage(true));
            return;
        }

        final var message = desired.entrySet().stream()
                .map(entry -> {
                    final var flip = entry.getKey();
                    return flip.getMessage(entry.getValue());
                })
                .collect(Collectors.joining(""));

        transmit(message);
    }
}
