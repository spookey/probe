package su.toor.probe.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.DayOfWeek;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.FlipRepository;
import su.toor.probe.database.PointEntity;
import su.toor.probe.database.PointRepository;
import su.toor.probe.shared.Flip;

@Service
public class EntityService {

    private static final Logger LOG = LoggerFactory.getLogger(EntityService.class);

    static final String FIELD_FLIP_ENTITY = "flipEntity";
    static final String FIELD_POINT_ENTITY = "pointEntity";

    public static final DayOfWeek INITIAL_DAY = DayOfWeek.MONDAY;
    public static final int INITIAL_HOUR = 0;
    public static final int INITIAL_MINUTE = 0;
    public static final boolean INITIAL_TARGET = false;

    private final FlipRepository flipRepository;
    private final PointRepository pointRepository;

    @Autowired
    public EntityService(final FlipRepository flipRepository, final PointRepository pointRepository) {
        this.flipRepository = flipRepository;
        this.pointRepository = pointRepository;
    }

    FlipEntity update(final FlipEntity flipEntity) {
        LOG.info("updating flip entity [{}]", keyValue(FIELD_FLIP_ENTITY, flipEntity));
        return flipRepository.save(flipEntity);
    }

    PointEntity update(final PointEntity pointEntity) {
        LOG.info("updating point entity [{}]", keyValue(FIELD_POINT_ENTITY, pointEntity));
        return pointRepository.save(pointEntity);
    }

    public void remove(final PointEntity pointEntity) {
        LOG.info("deleting point entity [{}]", keyValue(FIELD_POINT_ENTITY, pointEntity));
        pointRepository.delete(pointEntity);
    }

    public FlipEntity getOrCreateFlipEntity(final Flip flip) {
        final var optionalFlipEntity = flipRepository.findByFlip(flip);
        if (optionalFlipEntity.isPresent()) {
            return optionalFlipEntity.get();
        }

        final var flipEntity = FlipEntity.with(flip, flip.getLabel(), null);
        LOG.info("creating flip entity [{}]", keyValue(FIELD_FLIP_ENTITY, flipEntity));
        return update(flipEntity);
    }

    public List<FlipEntity> getOrCreateFlipEntities() {
        return EnumSet.allOf(Flip.class).stream()
                .sorted(Comparator.comparingInt(Flip::ordinal))
                .map(this::getOrCreateFlipEntity)
                .toList();
    }

    public PointEntity createPointEntity(
            final FlipEntity flipEntity,
            final DayOfWeek day,
            final Integer hour,
            final Integer minute,
            final Boolean target) {
        final var pointEntity = PointEntity.with(flipEntity, day, hour, minute, target);
        LOG.info("creating point entity [{}]", keyValue(FIELD_POINT_ENTITY, pointEntity));
        return update(pointEntity);
    }

    public PointEntity createInitialPointEntity(final FlipEntity flipEntity) {
        return createPointEntity(flipEntity, INITIAL_DAY, INITIAL_HOUR, INITIAL_MINUTE, INITIAL_TARGET);
    }

    public List<PointEntity> getOrCreatePointEntities(final FlipEntity flipEntity) {
        final var pointEntities = pointRepository.allOfFlipEntity(flipEntity);
        if (!pointEntities.isEmpty()) {
            return pointEntities;
        }

        return List.of(createInitialPointEntity(flipEntity));
    }

    public FlipEntity changeFlipEntity(final FlipEntity flipEntity, final String label, final Boolean forced) {
        return update(flipEntity.setLabel(label).setForced(forced));
    }

    public Optional<PointEntity> locatePointEntity(
            final FlipEntity flipEntity,
            final DayOfWeek day,
            final Integer hour,
            final Integer minute,
            final Boolean target) {
        return pointRepository.locateExact(flipEntity, day, hour, minute, target);
    }

    public Optional<PointEntity> latestPointEntity(
            final FlipEntity flipEntity, final DayOfWeek day, final Integer hour, final Integer minute) {
        return pointRepository.locateBefore(flipEntity, day, hour, minute).stream()
                .findFirst();
    }
}
