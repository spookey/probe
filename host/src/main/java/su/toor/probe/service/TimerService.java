package su.toor.probe.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.toor.probe.appliaction.setting.BasicSetting;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;
import su.toor.probe.shared.Flip;

@Service
public class TimerService {

    private static final Logger LOG = LoggerFactory.getLogger(TimerService.class);

    static final String FIELD_FLIP = "flip";

    private final BasicSetting basicSetting;
    private final EntityService entityService;

    @Autowired
    public TimerService(final BasicSetting basicSetting, final EntityService entityService) {
        this.basicSetting = basicSetting;
        this.entityService = entityService;
    }

    ZonedDateTime getStamp() {
        final var zoneId = basicSetting.getTimezone();
        return ZonedDateTime.now(zoneId);
    }

    PointEntity searchPointEntity(final FlipEntity flipEntity, final ZonedDateTime stamp) {
        return entityService
                .latestPointEntity(flipEntity, stamp.getDayOfWeek(), stamp.getHour(), stamp.getMinute())
                .orElseGet(() -> entityService
                        .latestPointEntity(flipEntity, DayOfWeek.SUNDAY, 23, 59)
                        .orElseGet(() -> entityService.createInitialPointEntity(flipEntity)));
    }

    Optional<Map.Entry<Flip, Boolean>> determineDesired(final FlipEntity flipEntity, final ZonedDateTime stamp) {
        final var flip = flipEntity.getFlip();
        final var pointEntity = searchPointEntity(flipEntity, stamp);

        if (flip.isInternal()) {
            return Optional.empty();
        }

        if (flipEntity.getForced() != null) {
            final var target = flipEntity.getForced();
            LOG.info("forced state [{}] [{}]", keyValue(FIELD_FLIP, flip), keyValue("target", target));
            return Optional.of(Map.entry(flip, target));
        }

        return Optional.of(Map.entry(flip, pointEntity.getTarget()));
    }

    public Map<Flip, Boolean> determineDesired() {
        final var stamp = getStamp();

        return entityService.getOrCreateFlipEntities().stream()
                .map(flipEntity -> determineDesired(flipEntity, stamp))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
