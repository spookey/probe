package su.toor.probe.resource.model;

import java.time.DayOfWeek;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.IntStream;
import su.toor.probe.database.PointEntity;
import su.toor.probe.service.EntityService;
import su.toor.probe.shared.Target;

public record PointForm(DayOfWeek day, Integer hour, Integer minute, Target target) {

    public static PointForm from(final PointEntity pointEntity) {
        return new PointForm(
                pointEntity.getDay(),
                pointEntity.getHour(),
                pointEntity.getMinute(),
                Target.resolve(pointEntity.getTarget()));
    }

    public static PointForm empty() {
        return new PointForm(
                EntityService.INITIAL_DAY,
                EntityService.INITIAL_HOUR,
                EntityService.INITIAL_MINUTE,
                Target.resolve(EntityService.INITIAL_TARGET));
    }

    public static List<NamedElem<DayOfWeek>> days() {
        return NamedElem.from(
                EnumSet.allOf(DayOfWeek.class).stream().sorted(Comparator.comparingInt(DayOfWeek::ordinal)),
                CoreModel::formatDay);
    }

    public static List<NamedElem<Integer>> hours() {
        return NamedElem.from(IntStream.rangeClosed(0, 23).boxed(), CoreModel::formatNum);
    }

    public static List<NamedElem<Integer>> minutes() {
        return NamedElem.from(IntStream.rangeClosed(0, 59).boxed(), CoreModel::formatNum);
    }

    public static List<NamedElem<Target>> targets() {
        return NamedElem.from(
                EnumSet.allOf(Target.class).stream()
                        .filter(target -> !target.isEnforce())
                        .sorted(Comparator.comparingInt(Target::ordinal)),
                Target::getLabel);
    }
}
