package su.toor.probe.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import su.toor.probe.service.EntityService;
import su.toor.probe.shared.FlipDto;

@Timed
@Tag(name = "Glance", description = "Backup and restore")
@RestController
@RequestMapping(value = "/api/glance")
public class GlanceApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(GlanceApiResource.class);

    private final EntityService entityService;

    @Autowired
    public GlanceApiResource(final EntityService entityService) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "Backup all values", tags = "Glance")
    @ApiResponse(responseCode = "200", description = "Backup values")
    @GetMapping(value = "/all", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlipDto>> getGlance() {

        final var result = entityService.getOrCreateFlipEntities().stream()
                .map(flipEntity -> FlipDto.from(flipEntity, entityService.getOrCreatePointEntities(flipEntity)))
                .toList();

        LOG.info("export requested [{}]", keyValue("flips", result));

        return ResponseEntity.ok(result);
    }

    @Timed
    @Operation(summary = "Restore all values", tags = "Glance")
    @ApiResponse(responseCode = "200", description = "Restored values")
    @ApiResponse(responseCode = "400", description = "Broken payload syntax")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @PutMapping(value = "/all", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlipDto>> postGlance(@RequestBody final List<FlipDto> body) {
        for (final var flipDto : body) {
            final var flipEntity = entityService.changeFlipEntity(
                    entityService.getOrCreateFlipEntity(flipDto.flip()), flipDto.label(), flipDto.forced());
            entityService.getOrCreatePointEntities(flipEntity).forEach(entityService::remove);

            for (final var pointDto : flipDto.points()) {
                entityService.createPointEntity(
                        flipEntity, pointDto.day(), pointDto.hour(), pointDto.minute(), pointDto.target());
            }
        }

        return ResponseEntity.created(URI.create("/api/glance/all")).build();
    }
}
