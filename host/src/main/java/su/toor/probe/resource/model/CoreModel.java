package su.toor.probe.resource.model;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.Target;

public record CoreModel(String language, String charset, String applicationName) {
    public static final Locale LOCALE = Locale.ENGLISH;
    public static final Charset UNICODE = StandardCharsets.UTF_8;

    public static CoreModel with(final String applicationName) {
        return new CoreModel(LOCALE.getLanguage(), UNICODE.name().toLowerCase(), applicationName);
    }

    public static String formatState(final Boolean state) {
        return Target.resolve(state).getLabel();
    }

    public static String formatState(final Target target) {
        return formatState(target.getState());
    }

    public static String formatDay(final DayOfWeek day) {
        return day.getDisplayName(TextStyle.FULL, LOCALE);
    }

    public static String formatNum(final Integer num) {
        return "%02d".formatted(num);
    }

    public static List<NamedElem<Flip>> flips() {
        return NamedElem.from(
                EnumSet.allOf(Flip.class).stream()
                        .filter(flip -> !flip.isInternal())
                        .sorted(Comparator.comparingInt(Flip::ordinal)),
                Flip::getLabel);
    }
}
