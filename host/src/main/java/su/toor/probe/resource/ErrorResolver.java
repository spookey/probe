package su.toor.probe.resource;

import jakarta.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import su.toor.probe.resource.model.CoreModel;

@Component
public class ErrorResolver implements ErrorViewResolver {

    private final BuildProperties buildProperties;

    @Autowired
    public ErrorResolver(final BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Override
    public ModelAndView resolveErrorView(
            final HttpServletRequest request, final HttpStatus status, final Map<String, Object> model) {
        final var errorModel = new HashMap<>(model);
        errorModel.put("core", CoreModel.with(buildProperties.getName()));

        return new ModelAndView("error", errorModel, status);
    }
}
