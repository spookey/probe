package su.toor.probe.resource.model;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public record NamedElem<T>(T elem, String name) {

    public static <S> List<NamedElem<S>> from(final Stream<S> elems, final Function<S, String> nameGetter) {
        return elems.map(elem -> new NamedElem<>(elem, nameGetter.apply(elem))).toList();
    }
}
