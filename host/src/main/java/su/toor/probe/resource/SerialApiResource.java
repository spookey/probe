package su.toor.probe.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import su.toor.probe.service.SerialService;
import su.toor.probe.shared.StateDto;

@Timed
@Tag(name = "Serial", description = "Serial read access")
@RestController
@RequestMapping(value = "/api/serial", produces = APPLICATION_JSON_VALUE)
public class SerialApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(SerialApiResource.class);

    private final SerialService serialService;

    @Autowired
    public SerialApiResource(final SerialService serialService) {
        this.serialService = serialService;
    }

    @Timed
    @Operation(summary = "Current serial state", tags = "Serial")
    @ApiResponse(responseCode = "200", description = "Serial state transmitted")
    @GetMapping(value = "/state", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<StateDto> getState() {
        final var result = serialService.getState();

        LOG.info("state requested [{}]", keyValue("state", result));

        return ResponseEntity.ok(result);
    }
}
