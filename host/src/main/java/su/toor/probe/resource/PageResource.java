package su.toor.probe.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

import io.micrometer.core.annotation.Timed;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import su.toor.probe.resource.model.CoreModel;
import su.toor.probe.resource.model.FlipForm;
import su.toor.probe.resource.model.PointForm;
import su.toor.probe.service.EntityService;
import su.toor.probe.service.SerialService;
import su.toor.probe.shared.Flip;

@Timed
@Controller
@RequestMapping(value = "/", produces = TEXT_HTML_VALUE)
public class PageResource {

    private static final Logger LOG = LoggerFactory.getLogger(PageResource.class);

    static final String FIELD_STATE = "state";
    static final String FIELD_CORE = "core";
    static final String FIELD_FLIP = "flip";
    static final String FIELD_FLIP_FORM = "flipForm";
    static final String FIELD_POINT_FORM = "pointForm";
    static final String FIELD_POINT_FORMS = "pointForms";

    private final BuildProperties buildProperties;
    private final SerialService serialService;
    private final EntityService entityService;

    @Autowired
    public PageResource(
            final BuildProperties buildProperties,
            final SerialService serialService,
            final EntityService entityService) {
        this.buildProperties = buildProperties;
        this.serialService = serialService;
        this.entityService = entityService;
    }

    private static RedirectView redirectToFlipPage(final Flip flip) {
        final var view = new RedirectView("/flip/%s".formatted(flip));
        view.setContextRelative(true);
        view.setExposeModelAttributes(false);
        view.setStatusCode(HttpStatus.SEE_OTHER);
        return view;
    }

    @ModelAttribute(FIELD_CORE)
    public CoreModel emitCoreModel() {
        return CoreModel.with(buildProperties.getName());
    }

    @Timed
    @GetMapping(produces = TEXT_HTML_VALUE)
    public ModelAndView getHomePage() {
        LOG.info("home page requested");

        return new ModelAndView("home", Map.of(FIELD_STATE, serialService.getState()));
    }

    @Timed
    @GetMapping(value = "flip/{flip}", produces = TEXT_HTML_VALUE)
    public ModelAndView getFlipPage(@PathVariable(value = "flip") final Flip flip) {
        LOG.info("flip page requested [{}]", keyValue(FIELD_FLIP, flip));

        final var flipEntity = entityService.getOrCreateFlipEntity(flip);
        final var pointForms = entityService.getOrCreatePointEntities(flipEntity).stream()
                .map(PointForm::from)
                .toList();

        final var flipForm = FlipForm.from(flipEntity);
        final var pointForm = PointForm.empty();

        return new ModelAndView(
                "flip",
                Map.of(
                        FIELD_FLIP, flip,
                        FIELD_FLIP_FORM, flipForm,
                        FIELD_POINT_FORM, pointForm,
                        FIELD_POINT_FORMS, pointForms));
    }

    @Timed
    @PostMapping(value = "flip/{flip}/edit", produces = TEXT_HTML_VALUE, consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public RedirectView editFlipPage(
            @PathVariable(value = "flip") final Flip flip, @ModelAttribute final FlipForm flipForm) {
        LOG.info("edit flip page requested [{}] [{}]", keyValue(FIELD_FLIP, flip), keyValue(FIELD_FLIP_FORM, flipForm));

        final var flipEntity = entityService.getOrCreateFlipEntity(flip);
        entityService.changeFlipEntity(
                flipEntity, flipForm.label(), flipForm.forced().getState());

        return redirectToFlipPage(flip);
    }

    @Timed
    @PostMapping(
            value = "flip/{flip}/point/drop",
            produces = TEXT_HTML_VALUE,
            consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public RedirectView dropPointPage(
            @PathVariable(value = "flip") final Flip flip, @ModelAttribute final PointForm pointForm) {
        LOG.info(
                "drop point page requested [{}] [{}]",
                keyValue(FIELD_FLIP, flip),
                keyValue(FIELD_POINT_FORM, pointForm));

        final var flipEntity = entityService.getOrCreateFlipEntity(flip);
        entityService
                .locatePointEntity(
                        flipEntity,
                        pointForm.day(),
                        pointForm.hour(),
                        pointForm.minute(),
                        pointForm.target().getState())
                .ifPresentOrElse(
                        entityService::remove,
                        () -> LOG.error(
                                "cannot drop - not found [{}] [{}]",
                                keyValue(FIELD_FLIP, flip),
                                keyValue(FIELD_POINT_FORM, pointForm)));

        return redirectToFlipPage(flip);
    }

    @Timed
    @PostMapping(
            value = "flip/{flip}/point/plus",
            produces = TEXT_HTML_VALUE,
            consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public RedirectView plusPointPage(
            @PathVariable(value = "flip") final Flip flip, @ModelAttribute final PointForm pointForm) {
        LOG.info(
                "plus point page requested [{}] [{}]",
                keyValue(FIELD_FLIP, flip),
                keyValue(FIELD_POINT_FORM, pointForm));

        final var flipEntity = entityService.getOrCreateFlipEntity(flip);
        entityService.createPointEntity(
                flipEntity,
                pointForm.day(),
                pointForm.hour(),
                pointForm.minute(),
                pointForm.target().getState());

        return redirectToFlipPage(flip);
    }
}
