package su.toor.probe.resource.model;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.shared.Target;

public record FlipForm(String label, Target forced) {

    public static FlipForm from(final FlipEntity flipEntity) {
        return new FlipForm(flipEntity.getLabel(), Target.resolve(flipEntity.getForced()));
    }

    public static List<NamedElem<Target>> targets() {
        return NamedElem.from(
                EnumSet.allOf(Target.class).stream().sorted(Comparator.comparingInt(Target::ordinal)),
                Target::getLabel);
    }
}
