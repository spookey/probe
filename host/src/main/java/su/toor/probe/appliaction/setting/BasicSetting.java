package su.toor.probe.appliaction.setting;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "basic")
@Validated
public class BasicSetting {

    static final boolean DEFAULT_ALLOW_UNINITIALIZED_PORT = false;
    static final boolean DEFAULT_TEARDOWN_ON_PORT_DISCONNECT = true;
    static final ZoneId DEFAULT_TIMEZONE = ZoneOffset.UTC;

    private Boolean allowUninitializedPort;
    private Boolean teardownOnPortDisconnect;
    private ZoneId timezone;

    @NonNull public Boolean getAllowUninitializedPort() {
        return Optional.ofNullable(allowUninitializedPort).orElse(DEFAULT_ALLOW_UNINITIALIZED_PORT);
    }

    public BasicSetting setAllowUninitializedPort(final Boolean allowUninitializedPort) {
        this.allowUninitializedPort = allowUninitializedPort;
        return this;
    }

    @NonNull public Boolean getTeardownOnPortDisconnect() {
        return Optional.ofNullable(teardownOnPortDisconnect).orElse(DEFAULT_TEARDOWN_ON_PORT_DISCONNECT);
    }

    public BasicSetting setTeardownOnPortDisconnect(final Boolean teardownOnPortDisconnect) {
        this.teardownOnPortDisconnect = teardownOnPortDisconnect;
        return this;
    }

    @NonNull public ZoneId getTimezone() {
        return Optional.ofNullable(timezone).orElse(DEFAULT_TIMEZONE);
    }

    public BasicSetting setTimezone(final ZoneId timezone) {
        this.timezone = timezone;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("allowUninitializedPort", getAllowUninitializedPort())
                .append("teardownOnPortDisconnect", getTeardownOnPortDisconnect())
                .append("timezone", getTimezone())
                .toString();
    }
}
