package su.toor.probe.appliaction.setting.serial;

public enum SerialDataBits implements Serial {
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8);

    private final int value;

    SerialDataBits(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }
}
