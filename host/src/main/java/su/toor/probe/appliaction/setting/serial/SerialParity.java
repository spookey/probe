package su.toor.probe.appliaction.setting.serial;

import com.fazecast.jSerialComm.SerialPort;

public enum SerialParity implements Serial {
    NONE(SerialPort.NO_PARITY),
    ODD(SerialPort.ODD_PARITY),
    EVEN(SerialPort.EVEN_PARITY),
    MARK(SerialPort.MARK_PARITY),
    SPACE(SerialPort.SPACE_PARITY);

    private final int value;

    SerialParity(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }
}
