package su.toor.probe.appliaction.setting.serial;

import com.fazecast.jSerialComm.SerialPort;

public enum SerialStopBits implements Serial {
    ONE(SerialPort.ONE_STOP_BIT),
    ONE_POINT_FIVE(SerialPort.ONE_POINT_FIVE_STOP_BITS),
    TWO(SerialPort.TWO_STOP_BITS);

    private final int value;

    SerialStopBits(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }
}
