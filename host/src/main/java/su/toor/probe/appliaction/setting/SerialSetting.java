package su.toor.probe.appliaction.setting;

import java.util.Optional;
import java.util.function.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import su.toor.probe.appliaction.setting.serial.SerialDataBits;
import su.toor.probe.appliaction.setting.serial.SerialParity;
import su.toor.probe.appliaction.setting.serial.SerialStopBits;

@Configuration
@ConfigurationProperties(prefix = "serial")
@Validated
public class SerialSetting {

    static final int DEFAULT_BAUD = 115200;
    static final SerialDataBits DEFAULT_DATA_BITS = SerialDataBits.EIGHT;
    static final SerialParity DEFAULT_PARITY = SerialParity.NONE;
    static final SerialStopBits DEFAULT_STOP_BITS = SerialStopBits.ONE;

    private String port;
    private Integer baud;
    private SerialDataBits dataBits;
    private SerialParity parity;
    private SerialStopBits stopBits;

    @Nullable public String getPort() {
        return Optional.ofNullable(port).filter(Predicate.not(String::isBlank)).orElse(null);
    }

    public SerialSetting setPort(final String port) {
        this.port = port;
        return this;
    }

    @NonNull public Integer getBaud() {
        return Optional.ofNullable(baud).orElse(DEFAULT_BAUD);
    }

    public SerialSetting setBaud(final Integer baud) {
        this.baud = baud;
        return this;
    }

    @NonNull public SerialDataBits getDataBits() {
        return Optional.ofNullable(dataBits).orElse(DEFAULT_DATA_BITS);
    }

    public SerialSetting setDataBits(final SerialDataBits dataBits) {
        this.dataBits = dataBits;
        return this;
    }

    @NonNull public SerialParity getParity() {
        return Optional.ofNullable(parity).orElse(DEFAULT_PARITY);
    }

    public SerialSetting setParity(final SerialParity parity) {
        this.parity = parity;
        return this;
    }

    @NonNull public SerialStopBits getStopBits() {
        return Optional.ofNullable(stopBits).orElse(DEFAULT_STOP_BITS);
    }

    public SerialSetting setStopBits(final SerialStopBits stopBits) {
        this.stopBits = stopBits;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("port", getPort())
                .append("baud", getBaud())
                .append("dataBits", getDataBits())
                .append("parity", getParity())
                .append("stopBits", getStopBits())
                .toString();
    }
}
