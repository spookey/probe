package su.toor.probe.component;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.function.IntConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class TeardownComponent {

    private static final Logger LOG = LoggerFactory.getLogger(TeardownComponent.class);

    @Bean
    public TeardownHandler createTeardown(final ConfigurableApplicationContext applicationContext) {
        return new TeardownHandler(applicationContext, System::exit);
    }

    public static final class TeardownHandler {

        private final ConfigurableApplicationContext applicationContext;
        private final IntConsumer sysExit;

        TeardownHandler(final ConfigurableApplicationContext applicationContext, final IntConsumer sysExit) {
            this.applicationContext = applicationContext;
            this.sysExit = sysExit;
        }

        public void applicationQuit(final int exitCode) {
            final var effectiveExitCode = SpringApplication.exit(applicationContext, () -> exitCode);

            LOG.error("shutting down [{}]", keyValue("exitCode", exitCode));
            sysExit.accept(effectiveExitCode);
        }
    }
}
