package su.toor.probe.component;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.EnumSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import su.toor.probe.component.state.StateResolver;
import su.toor.probe.service.SerialService;

@Component
public class MeterComponent {

    private static final Logger LOG = LoggerFactory.getLogger(MeterComponent.class);

    static final String PROBE_STATE_NAME = "probe_state";
    static final String PROBE_STATE_DESCRIPTION = "Current state on the probe";
    static final String PROBE_STATE_TAG_TYPE = "type";
    static final String PROBE_STATE_TAG_NAME = "name";

    private final MeterRegistry meterRegistry;
    private final SerialService serialService;

    @Autowired
    public MeterComponent(final MeterRegistry meterRegistry, final SerialService serialService) {
        this.meterRegistry = meterRegistry;
        this.serialService = serialService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void registerMetrics() {
        LOG.info("registering gauges for probe state");

        EnumSet.allOf(StateResolver.class)
                .forEach(state -> Gauge.builder(PROBE_STATE_NAME, serialService.getState(), state.getResolver())
                        .tag(PROBE_STATE_TAG_TYPE, state.getTagType())
                        .tag(PROBE_STATE_TAG_NAME, state.getTagName())
                        .description(PROBE_STATE_DESCRIPTION)
                        .register(meterRegistry));
    }
}
