package su.toor.probe.component;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import su.toor.probe.service.SerialService;
import su.toor.probe.service.TimerService;

@EnableScheduling
@Component
public class WorkerComponent {

    private static final Logger LOG = LoggerFactory.getLogger(WorkerComponent.class);

    static final long INTERVAL_TRANSMISSION_INITIAL = 1000L * 5L;
    static final long INTERVAL_TRANSMISSION_REGULAR = 1000L * 20L;

    private final TimerService timerService;
    private final SerialService serialService;

    public WorkerComponent(final TimerService timerService, final SerialService serialService) {
        this.timerService = timerService;
        this.serialService = serialService;
    }

    @Scheduled(initialDelay = INTERVAL_TRANSMISSION_INITIAL, fixedRate = INTERVAL_TRANSMISSION_REGULAR)
    public void sendTransmission() {
        final var desired = timerService.determineDesired();
        LOG.info("transmitting [{}]", keyValue("desired", desired));
        serialService.transmit(desired);
    }
}
