package su.toor.probe.component.state;

import java.util.Optional;
import java.util.function.ToDoubleFunction;
import su.toor.probe.shared.StateDto;

public enum StateResolver {
    FLIP_1(dto -> from(dto.getFlip1()), "flip_1", "flip"),
    FLIP_2(dto -> from(dto.getFlip2()), "flip_2", "flip"),
    FLIP_3(dto -> from(dto.getFlip3()), "flip_3", "flip"),
    FLIP_4(dto -> from(dto.getFlip4()), "flip_4", "flip"),
    READ_1(dto -> from(dto.getRead1()), "read_1", "read"),
    READ_2(dto -> from(dto.getRead2()), "read_2", "read"),
    SENS_1(dto -> from(dto.getSens1()), "sens_1", "sens"),
    SENS_2(dto -> from(dto.getSens2()), "sens_2", "sens"),
    UPTIME(dto -> from(dto.getUpNum()), "uptime", "uptime");

    private final ToDoubleFunction<StateDto> resolver;
    private final String tagName;
    private final String tagType;

    StateResolver(final ToDoubleFunction<StateDto> resolver, final String tagName, final String tagType) {
        this.resolver = resolver;
        this.tagName = tagName;
        this.tagType = tagType;
    }

    public ToDoubleFunction<StateDto> getResolver() {
        return resolver;
    }

    public String getTagName() {
        return tagName;
    }

    public String getTagType() {
        return tagType;
    }

    static double from(final Boolean value) {
        return Optional.ofNullable(value).filter(val -> val).map(val -> 1d).orElse(0d);
    }

    static double from(final Number value) {
        return Optional.ofNullable(value).map(Number::doubleValue).orElse(0d);
    }
}
