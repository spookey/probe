package su.toor.probe.shared.misc;

import static net.logstash.logback.argument.StructuredArguments.value;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

public final class Require {
    private Require() {}

    private static final Logger LOG = LoggerFactory.getLogger(Require.class);

    public static class RequireException extends RuntimeException {
        public RequireException(final String message) {
            super(message);
        }
    }

    static <T> T condition(final Predicate<T> check, final String message, final T value)
            throws IllegalArgumentException, RequireException {
        if (Objects.isNull(check)) {
            throw new IllegalArgumentException("check is null");
        }
        if (Optional.ofNullable(message).filter(Predicate.not(String::isBlank)).isEmpty()) {
            throw new IllegalArgumentException("message is null, empty or blank");
        }
        if (!check.test(value)) {
            final var exception = new RequireException(message);
            LOG.error("{} [{}]", value("message", message), value("value", value), exception);
            throw exception;
        }
        return value;
    }

    @NonNull public static <T> T notNull(final String fieldName, final T value) throws RequireException {
        return condition(Objects::nonNull, fieldName + " is null", value);
    }

    @NonNull public static Integer betweenIncluding(
            final String fieldName, final Integer number, final Integer lowerIncluding, final Integer upperIncluding)
            throws RequireException {
        notNull(fieldName, number);
        notNull("lowerIncluding", lowerIncluding);
        notNull("upperIncluding", upperIncluding);

        condition(ignore -> lowerIncluding <= upperIncluding, "Idiot!", null);
        condition(num -> num >= lowerIncluding, fieldName + " is too low", number);
        condition(num -> num <= upperIncluding, fieldName + " is too high", number);
        return number;
    }
}
