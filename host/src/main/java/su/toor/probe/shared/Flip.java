package su.toor.probe.shared;

public enum Flip {
    FLIP_1("1f", "1n", false, "Flip 1"),
    FLIP_2("2f", "2n", false, "Flip 2"),
    FLIP_3("3f", "3n", false, "Flip 3"),
    FLIP_4("4f", "4n", false, "Flip 4"),
    UPDATE("::", "..", true, "Keepalive");

    private final String full;
    private final String none;
    private final boolean internal;
    private final String label;

    Flip(final String full, final String none, final boolean internal, final String label) {
        this.full = full;
        this.none = none;
        this.internal = internal;
        this.label = label;
    }

    public String getMessage(final boolean target) {
        return target ? full : none;
    }

    public boolean isInternal() {
        return internal;
    }

    public String getLabel() {
        return label;
    }
}
