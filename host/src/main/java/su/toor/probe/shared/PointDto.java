package su.toor.probe.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.DayOfWeek;
import su.toor.probe.database.PointEntity;
import su.toor.probe.shared.misc.Require;

public record PointDto(
        @JsonProperty("day") @Schema(name = "day", description = "Day of week") DayOfWeek day,
        @JsonProperty("hour") @Schema(name = "hour", description = "Hour of day") Integer hour,
        @JsonProperty("minute") @Schema(name = "minute", description = "Minute of hour") Integer minute,
        @JsonProperty("target") @Schema(name = "target", description = "Desired state") Boolean target) {
    public PointDto {
        Require.notNull("day", day);
        Require.betweenIncluding("hour", hour, 0, 23);
        Require.betweenIncluding("minute", minute, 0, 59);
        Require.notNull("target", target);
    }

    public static PointDto from(final PointEntity pointEntity) {
        return new PointDto(
                pointEntity.getDay(), pointEntity.getHour(), pointEntity.getMinute(), pointEntity.getTarget());
    }
}
