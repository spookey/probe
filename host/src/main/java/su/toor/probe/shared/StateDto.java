package su.toor.probe.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Objects;
import java.util.Optional;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;

public class StateDto {

    @Schema(name = "flip1", description = "Digital output #1")
    @JsonProperty("flip1")
    private boolean flip1 = false;

    @Schema(name = "flip2", description = "Digital output #2")
    @JsonProperty("flip2")
    private boolean flip2 = false;

    @Schema(name = "flip3", description = "Digital output #3")
    @JsonProperty("flip3")
    private boolean flip3 = false;

    @Schema(name = "flip4", description = "Digital output #4")
    @JsonProperty("flip4")
    private boolean flip4 = false;

    @Schema(name = "read1", description = "Digital input #1")
    @JsonProperty("read1")
    private boolean read1 = false;

    @Schema(name = "read2", description = "Digital input #2")
    @JsonProperty("read2")
    private boolean read2 = false;

    @Schema(name = "sens1", description = "Analog input #1")
    @JsonProperty("sens1")
    private int sens1 = 0;

    @Schema(name = "sens2", description = "Analog input #2")
    @JsonProperty("sens2")
    private int sens2 = 0;

    @Schema(name = "upNum", description = "Node uptime in seconds")
    @JsonProperty("upNum")
    private long upNum = 0L;

    @Schema(name = "upStr", description = "Node uptime as formatted string")
    @JsonProperty("upStr")
    private String upStr = "";

    public boolean getFlip1() {
        return flip1;
    }

    public StateDto setFlip1(final boolean flip1) {
        this.flip1 = flip1;
        return this;
    }

    public boolean getFlip2() {
        return flip2;
    }

    public StateDto setFlip2(final boolean flip2) {
        this.flip2 = flip2;
        return this;
    }

    public boolean getFlip3() {
        return flip3;
    }

    public StateDto setFlip3(final boolean flip3) {
        this.flip3 = flip3;
        return this;
    }

    public boolean getFlip4() {
        return flip4;
    }

    public StateDto setFlip4(final boolean flip4) {
        this.flip4 = flip4;
        return this;
    }

    public boolean getRead1() {
        return read1;
    }

    public StateDto setRead1(final boolean read1) {
        this.read1 = read1;
        return this;
    }

    public boolean getRead2() {
        return read2;
    }

    public StateDto setRead2(final boolean read2) {
        this.read2 = read2;
        return this;
    }

    public int getSens1() {
        return sens1;
    }

    public StateDto setSens1(final int sens1) {
        this.sens1 = sens1;
        return this;
    }

    public int getSens2() {
        return sens2;
    }

    public StateDto setSens2(final int sens2) {
        this.sens2 = sens2;
        return this;
    }

    public long getUpNum() {
        return upNum;
    }

    public StateDto setUpNum(final long upNum) {
        this.upNum = upNum;
        return this;
    }

    @NonNull public String getUpStr() {
        return Optional.ofNullable(upStr).orElse("");
    }

    public StateDto setUpStr(final String upStr) {
        this.upStr = Optional.ofNullable(upStr).orElse("");
        return this;
    }

    @JsonIgnore
    public StateDto set(final StateDto state) {
        return this.setFlip1(state.getFlip1())
                .setFlip2(state.getFlip2())
                .setFlip3(state.getFlip3())
                .setFlip4(state.getFlip4())
                .setRead1(state.getRead1())
                .setRead2(state.getRead2())
                .setSens1(state.getSens1())
                .setSens2(state.getSens2())
                .setUpNum(state.getUpNum())
                .setUpStr(state.getUpStr());
    }

    @Override
    public int hashCode() {
        return Objects.hash(flip1, flip2, flip3, flip4, read1, read2, sens1, sens2, upNum, upStr);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof final StateDto that)) {
            return false;
        }

        return this.flip1 == that.flip1
                && this.flip2 == that.flip2
                && this.flip3 == that.flip3
                && this.flip4 == that.flip4
                && this.read1 == that.read1
                && this.read2 == that.read2
                && this.sens1 == that.sens1
                && this.sens2 == that.sens2
                && this.upNum == that.upNum
                && Objects.equals(this.upStr, that.upStr);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("flip1", getFlip1())
                .append("flip2", getFlip2())
                .append("flip3", getFlip3())
                .append("flip4", getFlip4())
                .append("read1", getRead1())
                .append("read2", getRead2())
                .append("sens1", getSens1())
                .append("sens2", getSens2())
                .append("upNum", getUpNum())
                .append("upStr", getUpStr())
                .toString();
    }
}
