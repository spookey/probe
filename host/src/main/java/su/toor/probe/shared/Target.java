package su.toor.probe.shared;

import java.util.EnumSet;
import java.util.Optional;

public enum Target {
    TIMED("Timed", null, true),
    OFF("Off", false, false),
    ON("On", true, false);

    private final String label;
    private final Boolean state;
    private final boolean enforce;

    Target(final String label, final Boolean state, final boolean enforce) {
        this.label = label;
        this.state = state;
        this.enforce = enforce;
    }

    public static Target resolve(final Boolean state) {
        return Optional.ofNullable(state)
                .flatMap(sta -> EnumSet.allOf(Target.class).stream()
                        .filter(target -> sta.equals(target.getState()))
                        .findFirst())
                .orElse(Target.TIMED);
    }

    public String getLabel() {
        return label;
    }

    public Boolean getState() {
        return state;
    }

    public boolean isEnforce() {
        return enforce;
    }
}
