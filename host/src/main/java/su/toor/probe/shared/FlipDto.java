package su.toor.probe.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Collection;
import java.util.List;
import org.springframework.lang.Nullable;
import su.toor.probe.database.FlipEntity;
import su.toor.probe.database.PointEntity;
import su.toor.probe.shared.misc.Require;

public record FlipDto(
        @JsonProperty("flip") @Schema(name = "flip", description = "Flip name") Flip flip,
        @JsonProperty("label")
                @Schema(name = "label", description = "Optional label of flip", nullable = true)
                @Nullable String label,
        @JsonProperty("forced")
                @Schema(name = "force", description = "Overwrite point targets", nullable = true)
                @Nullable Boolean forced,
        @JsonProperty("points") @Schema(name = "points", description = "Point entries") List<PointDto> points) {
    public FlipDto {
        Require.notNull("flip", flip);
        Require.notNull("points", points);
    }

    public static FlipDto from(final FlipEntity flipEntity, final Collection<PointEntity> pointEntities) {
        return new FlipDto(
                flipEntity.getFlip(),
                flipEntity.getLabel(),
                flipEntity.getForced(),
                pointEntities.stream().map(PointDto::from).toList());
    }
}
