package su.toor.probe.communication.core;

import java.nio.charset.StandardCharsets;
import org.springframework.lang.NonNull;
import su.toor.probe.shared.StateDto;

public interface Communication {

    void teardown();

    void setState(final StateDto state);

    String CRLF = "\r\n";

    static byte[] asBytes(@NonNull final String content) {
        return content.getBytes(StandardCharsets.UTF_8);
    }
}
