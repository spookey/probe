package su.toor.probe.communication.core;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import su.toor.probe.shared.StateDto;

public final class MessageListener implements SerialPortMessageListener {

    private static final Logger LOG = LoggerFactory.getLogger(MessageListener.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final Communication communicator;

    public MessageListener(final Communication communicator) {
        this.communicator = communicator;
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_PORT_DISCONNECTED ^ SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    @Override
    public byte[] getMessageDelimiter() {
        return Communication.asBytes(Communication.CRLF);
    }

    @Override
    public boolean delimiterIndicatesEndOfMessage() {
        return true;
    }

    @Override
    public void serialEvent(final SerialPortEvent event) {
        final var eventType = event.getEventType();
        if (eventType == SerialPort.LISTENING_EVENT_PORT_DISCONNECTED) {
            LOG.warn("disconnect event received");
            communicator.teardown();
            return;
        }
        if (eventType != SerialPort.LISTENING_EVENT_DATA_RECEIVED) {
            return;
        }

        final var data = event.getReceivedData();
        try {
            final var state = MAPPER.readValue(data, StateDto.class);

            LOG.info("received new state [{}]", keyValue("state", state));

            communicator.setState(state);
        } catch (final IOException ex) {
            LOG.debug("error parsing state", ex);
        }
    }
}
