package su.toor.probe.communication.core.ex;

import java.util.Optional;
import org.springframework.lang.Nullable;

public class UninitializedException extends Exception {

    static final String UNKNOWN = "unknown";

    public UninitializedException(@Nullable final String systemPortPath) {
        super("attempted operation on closed port - initialize it first [%s]"
                .formatted(Optional.ofNullable(systemPortPath).orElse(UNKNOWN)));
    }
}
