package su.toor.probe.communication.core;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import su.toor.probe.appliaction.setting.SerialSetting;

@Component
public class PortComponent {

    private static final Logger LOG = LoggerFactory.getLogger(PortComponent.class);

    @Bean
    public SerialPort createSerialPort(final SerialSetting serialSetting) {
        LOG.info("creating serial port [{}]", keyValue("serialSetting", serialSetting));

        final var port = Optional.ofNullable(serialSetting.getPort())
                .orElseThrow(() -> new BeanInstantiationException(PortComponent.class, "port is not configured"));
        final var dataBits = serialSetting.getDataBits();
        final var parity = serialSetting.getParity();
        final var stopBits = serialSetting.getStopBits();

        final SerialPort serialPort;
        try {
            serialPort = SerialPort.getCommPort(port);
        } catch (final SerialPortInvalidPortException ex) {
            throw new BeanInstantiationException(PortComponent.class, "cannot connect to port", ex);
        }

        serialPort.setBaudRate(serialSetting.getBaud());
        serialPort.setNumDataBits(dataBits.getValue());
        serialPort.setParity(parity.getValue());
        serialPort.setNumStopBits(stopBits.getValue());

        LOG.info("created port [{}]", keyValue("serialPort", serialPort));

        return serialPort;
    }
}
