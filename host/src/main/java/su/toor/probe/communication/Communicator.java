package su.toor.probe.communication;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import com.fazecast.jSerialComm.SerialPort;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import su.toor.probe.appliaction.setting.BasicSetting;
import su.toor.probe.communication.core.Communication;
import su.toor.probe.communication.core.MessageListener;
import su.toor.probe.communication.core.ex.UninitializedException;
import su.toor.probe.communication.core.ex.WriterException;
import su.toor.probe.component.TeardownComponent.TeardownHandler;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.StateDto;

@Component(value = "nonInitializedCommunicator")
public class Communicator implements Communication {

    private static final Logger LOG = LoggerFactory.getLogger(Communicator.class);

    static final String FIELD_CONTENT = "content";
    static final String FIELD_MESSAGE_LISTENER = "messageListener";
    static final String FIELD_SERIAL_PORT = "serialPort";

    static final int PORT_DISCONNECT_EXIT_CODE = 23;

    private final BasicSetting basicSetting;
    private final SerialPort serialPort;
    private final TeardownHandler teardownHandler;
    private final AtomicBoolean initialized;
    private final MessageListener messageListener;
    private final StateDto state;

    Communicator(
            final BasicSetting basicSetting,
            final SerialPort serialPort,
            final TeardownHandler teardownHandler,
            final AtomicBoolean initialized) {
        this.basicSetting = basicSetting;
        this.serialPort = serialPort;
        this.teardownHandler = teardownHandler;
        this.initialized = initialized;
        this.messageListener = new MessageListener(this);
        this.state = new StateDto();
    }

    @Bean(name = "initialized")
    static AtomicBoolean atomicBoolean() {
        return new AtomicBoolean(false);
    }

    @Bean
    @Primary
    public static Communicator communicator(
            final BasicSetting basicSetting,
            final SerialPort serialPort,
            final TeardownHandler teardownHandler,
            final AtomicBoolean initialized)
            throws UninitializedException {
        final var communicator = new Communicator(basicSetting, serialPort, teardownHandler, initialized);
        if (!communicator.open()) {
            throw new BeanInstantiationException(Communicator.class, "serial connection failed");
        }
        if (!communicator.register()) {
            throw new BeanInstantiationException(Communicator.class, "serial registration failed");
        }
        return communicator;
    }

    boolean open() {
        if (initialized.get()) {
            LOG.error("port is already initialized [{}]", keyValue(FIELD_SERIAL_PORT, serialPort));
            return false;
        }

        if (!serialPort.openPort()) {
            if (basicSetting.getAllowUninitializedPort()) {
                LOG.warn("cannot open serial port - ignored by config [{}]", keyValue(FIELD_SERIAL_PORT, serialPort));
                return true;
            }

            LOG.error("cannot open serial port [{}]", keyValue(FIELD_SERIAL_PORT, serialPort));
            return false;
        }

        initialized.set(serialPort.isOpen());
        return initialized.get();
    }

    boolean register() throws UninitializedException {
        if (!initialized.get()) {
            return basicSetting.getAllowUninitializedPort();
        }

        if (!serialPort.addDataListener(messageListener)) {
            LOG.error(
                    "cannot attach listener [{}] [{}]",
                    keyValue(FIELD_SERIAL_PORT, serialPort),
                    keyValue(FIELD_MESSAGE_LISTENER, messageListener));
            return false;
        }

        try {
            message(Flip.UPDATE.getMessage(false));
        } catch (final UninitializedException | WriterException ex) {
            LOG.error("cannot send initial message [{}]", keyValue(FIELD_SERIAL_PORT, serialPort), ex);
            return false;
        }

        return initialized.get();
    }

    @Override
    public void teardown() {
        initialized.set(false);

        if (!basicSetting.getTeardownOnPortDisconnect()) {
            LOG.warn("ignoring disconnect request");
            return;
        }

        teardownHandler.applicationQuit(PORT_DISCONNECT_EXIT_CODE);
    }

    public StateDto getState() {
        return state;
    }

    @Override
    public void setState(final StateDto state) {
        this.state.set(state);
    }

    public void message(final String content) throws UninitializedException, WriterException {
        if (!initialized.get()) {
            throw new UninitializedException(serialPort.getSystemPortPath());
        }

        final var payload = Communication.asBytes(content);
        final var finalize = Communication.asBytes(Communication.CRLF);

        LOG.info("sending message [{}]", keyValue(FIELD_CONTENT, content));
        try (final var stream = serialPort.getOutputStream()) {
            stream.write(payload);
            stream.write(finalize);
        } catch (final IOException ex) {
            throw new WriterException(ex);
        }
    }
}
