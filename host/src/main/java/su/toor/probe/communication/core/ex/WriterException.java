package su.toor.probe.communication.core.ex;

import java.io.IOException;

public class WriterException extends Exception {

    public WriterException(final IOException cause) {
        super(cause);
    }
}
