package su.toor.probe.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.time.DayOfWeek;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import su.toor.probe.shared.misc.Require;

@Entity
@Table(
        name = "point",
        indexes = {
            @Index(name = "idx_flip_id", columnList = "flip_id"),
            @Index(name = "idx_flip_id_day_hour_minute", columnList = "flip_id, day, hour, minute"),
            @Index(name = "idx_flip_id_day_hour_minute_target", columnList = "flip_id, day, hour, minute, target"),
        })
@NamedEntityGraph(name = "PointEntity.flipEntity", attributeNodes = @NamedAttributeNode("flipEntity"))
public class PointEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "flip_id", nullable = false, foreignKey = @ForeignKey(name = "fk_flip_flip_id"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private FlipEntity flipEntity;

    @Column(name = "day", updatable = false, nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private DayOfWeek day;

    @Column(name = "hour", updatable = false, nullable = false)
    private Integer hour;

    @Column(name = "minute", updatable = false, nullable = false)
    private Integer minute;

    @Column(name = "target", updatable = false, nullable = false)
    private Boolean target;

    @Transient
    public static PointEntity with(
            final FlipEntity flipEntity,
            final DayOfWeek day,
            final Integer hour,
            final Integer minute,
            final Boolean target) {
        final var entity = new PointEntity();
        entity.flipEntity = Require.notNull("flipEntity", flipEntity);
        entity.day = Require.notNull("day", day);
        entity.hour = Require.betweenIncluding("hour", hour, 0, 23);
        entity.minute = Require.betweenIncluding("minute", minute, 0, 59);
        entity.target = Require.notNull("target", target);
        return entity;
    }

    public Long getId() {
        return id;
    }

    public FlipEntity getFlipEntity() {
        return flipEntity;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public Integer getHour() {
        return hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public Boolean getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("flipEntity", getFlipEntity())
                .append("day", getDay())
                .append("hour", getHour())
                .append("minute", getMinute())
                .append("target", getTarget())
                .toString();
    }
}
