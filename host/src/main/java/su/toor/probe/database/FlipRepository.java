package su.toor.probe.database;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import su.toor.probe.shared.Flip;

public interface FlipRepository extends JpaRepository<FlipEntity, Long> {

    Optional<FlipEntity> findByFlip(final Flip flip);
}
