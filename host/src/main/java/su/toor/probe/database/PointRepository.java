package su.toor.probe.database;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PointRepository extends JpaRepository<PointEntity, Long> {

    @EntityGraph(value = "PointEntity.flipEntity")
    List<PointEntity> findAllByFlipEntityOrderByDayAscHourAscMinuteAscTargetAsc(final FlipEntity flipEntity);

    default List<PointEntity> allOfFlipEntity(final FlipEntity flipEntity) {
        return findAllByFlipEntityOrderByDayAscHourAscMinuteAscTargetAsc(flipEntity);
    }

    @EntityGraph(value = "PointEntity.flipEntity")
    Optional<PointEntity> findFirstByFlipEntityAndDayAndHourAndMinuteAndTarget(
            final FlipEntity flipEntity,
            final DayOfWeek day,
            final Integer hour,
            final Integer minute,
            final Boolean target);

    default Optional<PointEntity> locateExact(
            final FlipEntity flipEntity,
            final DayOfWeek day,
            final Integer hour,
            final Integer minute,
            final Boolean target) {
        return findFirstByFlipEntityAndDayAndHourAndMinuteAndTarget(flipEntity, day, hour, minute, target);
    }

    @Query(" "
            + "SELECT point FROM PointEntity point "
            + "WHERE point.flipEntity = :flipEntity "
            + "AND ( "
            + "      (point.day < :day) "
            + "   OR (point.day = :day AND point.hour < :hour) "
            + "   OR (point.day = :day AND point.hour = :hour AND point.minute <= :minute) "
            + ") "
            + "ORDER BY point.day DESC, point.hour DESC, point.minute DESC ")
    @EntityGraph(value = "PointEntity.flipEntity")
    List<PointEntity> locateBefore(
            @Param("flipEntity") final FlipEntity flipEntity,
            @Param("day") final DayOfWeek day,
            @Param("hour") final Integer hour,
            @Param("minute") final Integer minute);
}
