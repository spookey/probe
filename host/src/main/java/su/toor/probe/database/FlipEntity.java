package su.toor.probe.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import java.util.Optional;
import org.springframework.core.style.ToStringCreator;
import su.toor.probe.shared.Flip;
import su.toor.probe.shared.misc.Require;

@Entity
@Table(name = "flip", uniqueConstraints = @UniqueConstraint(name = "cst_flip", columnNames = "flip"))
public class FlipEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "flip", updatable = false, nullable = false)
    @Enumerated(EnumType.STRING)
    private Flip flip;

    @Column(name = "label", nullable = false)
    private String label;

    @Column(name = "forced")
    private Boolean forced;

    @Transient
    public static FlipEntity with(final Flip flip, final String label, final Boolean forced) {
        final var entity = new FlipEntity();
        entity.flip = Require.notNull("flip", flip);
        return entity.setLabel(label).setForced(forced);
    }

    @Transient
    public static FlipEntity with(final Flip flip, final String label) {
        return with(flip, label, null);
    }

    public Long getId() {
        return id;
    }

    public Flip getFlip() {
        return flip;
    }

    public String getLabel() {
        return label;
    }

    public FlipEntity setLabel(final String label) {
        this.label = Optional.ofNullable(label).orElseGet(flip::getLabel);
        return this;
    }

    public Boolean getForced() {
        return forced;
    }

    public FlipEntity setForced(final Boolean forced) {
        this.forced = forced;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("flip", getFlip())
                .append("label", getLabel())
                .append("forced", getForced())
                .toString();
    }
}
